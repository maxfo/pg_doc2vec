#include <dirent.h>

#include <string>
#include <vector>
#include <set>
#include <fstream>
#include <iostream>

#include <word2vec.h>

struct modelStat_t {
    const std::string modelName;
    float threshold = 0.0f;
    uint32_t falsePositive = 0;
    uint32_t falseNegative = 0;
    float avgPositive = 0.0f;
    float avgNegative = 0.0f;
    float minPositive = 1.0f;
    float maxNegative = 0.0f;
    float precision = 0.0f;
    float recall = 0.0f;
    float F1Score = 0.0f;
    float accuracy = 0.0f;

    explicit modelStat_t(std::string _name): modelName(std::move(_name)) {}

    friend bool operator<(const modelStat_t &_what, const modelStat_t &_with) {
        if ((_what.F1Score < _with.F1Score)
            || ((_what.F1Score == _with.F1Score) && (_what.accuracy < _with.accuracy))) {
            return true;
        }
        // undefined
        return false;
    }
};

class mdlTester_t {
private:
    std::unique_ptr<w2v::w2vModel_t> m_w2vModel;

public:
    explicit mdlTester_t(const std::string &_fileName): m_w2vModel() {
        // load w2v model file
        m_w2vModel.reset(new w2v::w2vModel_t());
        if (!m_w2vModel->load(_fileName)) {
            throw std::runtime_error(m_w2vModel->errMsg());
        }
    }
    ~mdlTester_t() = default;

    float distance(const std::string &_fileName1, const std::string &_fileName2, bool _withDigits = true) {
        // load docs
        std::string data1, data2;
        readFile(_fileName1, data1);
        readFile(_fileName2, data2);

        std::string eow;
        if (_withDigits) {
            eow = " \n,.-!?:;/\"#$%&'()*+<=>@[]\\^_`{|}~\t\v\f\r";
        } else {
            eow = " \n,.-!?:;/\"#$%&'()*+<=>@[]\\^_`{|}~\t\v\f\r1234567890";
        }
        w2v::doc2vec_t doc1(m_w2vModel, data1, eow);
        w2v::doc2vec_t doc2(m_w2vModel, data2, eow);

        w2v::d2vModel_t d2vModel(m_w2vModel->vectorSize());
        return d2vModel.distance(doc1, doc2);
    }

private:
    void readFile(const std::string &_fileName, std::string &_data) {
        std::ifstream ifs;
        ifs.open(_fileName.c_str());
        if (ifs.fail()) {
            throw std::runtime_error((std::string("failed to open file: ") + _fileName).c_str());
        }
        ifs.seekg(0, ifs.end);
        auto size = ifs.tellg();
        _data.resize(static_cast<std::size_t>(size), 0);
        ifs.seekg(0, ifs.beg);
        ifs.read(&_data[0], size);
        ifs.close();
    }
};

class modelsLoader_t {
public:
    using modelFiles_t = std::vector<std::string>;

private:
    modelFiles_t m_modelFiles;

public:
    explicit modelsLoader_t(const std::string &_modelsDir) {
        // load model file names
        DIR *dir = nullptr;
        if ((dir = opendir(_modelsDir.c_str())) != nullptr) {
            struct dirent *ent = nullptr;
            while ((ent = readdir(dir)) != nullptr) {
                std::string fileName = std::string(ent->d_name);
                if ((fileName == ".") || (fileName == "..") || (ent->d_type != DT_REG)) {
                    continue;
                }
                m_modelFiles.push_back(_modelsDir + "/" + ent->d_name);
            }
            closedir(dir);
        } else {
            throw std::runtime_error((std::string("failed to open ") + _modelsDir).c_str());
        }
    }

    const modelFiles_t &modelFiles() const {
        return m_modelFiles;
    }
};

class samplesLoader_t {
public:
    using filePairs_t = std::vector<std::pair<std::string, std::string>>;

private:
    const std::string m_samplesDir;
    filePairs_t m_positivePairs;
    filePairs_t m_negativePairs;

public:
    explicit samplesLoader_t(std::string _samplesDir): m_samplesDir(std::move(_samplesDir)), m_positivePairs(), m_negativePairs() {
        load(true);
        load(false);
    }
    ~samplesLoader_t() = default;

    const filePairs_t &positivePairs() const {
        return m_positivePairs;
    }
    const filePairs_t &negativePairs() const {
        return m_negativePairs;
    }

private:
    void load(bool _positive) {
        // positive samples
        std::vector<std::string> positiveDirs;
        std::string positiveSamplesDir = m_samplesDir + (_positive?"/positive":"/negative");
        DIR *dir = nullptr;
        if ((dir = opendir(positiveSamplesDir.c_str())) != nullptr) {
            struct dirent *ent = nullptr;
            while ((ent = readdir(dir)) != nullptr) {
                {
                    std::string fileName = std::string(ent->d_name);
                    if ((fileName == ".") || (fileName == "..") || (ent->d_type != DT_DIR)) {
                        continue;
                    }
                }
                DIR *subdir = nullptr;
                if ((subdir = opendir((positiveSamplesDir + "/" + ent->d_name).c_str())) != nullptr) {
                    struct dirent *subent = nullptr;
                    std::vector<std::string> sampleFiles;
                    while ((subent = readdir(subdir)) != nullptr) {
                        if (subent->d_type != DT_REG) {
                            continue;
                        }
                        sampleFiles.push_back(positiveSamplesDir + "/" + ent->d_name + "/" + subent->d_name);
                    }
                    if (sampleFiles.size() != 2) {
                        std::cerr << "Two files needed, skipping folder "
                                  << positiveSamplesDir + "/" + ent->d_name << std::endl;
                        continue;
                    }
                    if (_positive) {
                        m_positivePairs.emplace_back(std::pair<std::string, std::string>(sampleFiles[0],
                                                                                         sampleFiles[1]));
                    } else {
                        m_negativePairs.emplace_back(std::pair<std::string, std::string>(sampleFiles[0],
                                                                                         sampleFiles[1]));
                    }
                }
                closedir(subdir);
            }
            closedir(dir);
        } else {
            throw std::runtime_error((std::string("failed to open ") + m_samplesDir).c_str());
        }
    }
};

int main(int argc, char * const *argv) {
    if (((argc < 3) || (argc > 4))  || ((argc == 4) && (std::string(argv[3]) != "verbose"))) {
        std::cerr << "Usage:" << std::endl
                  << argv[0] << " [w2v_dir] [samples_dir] verbose" << std::endl;
        return EXIT_FAILURE;
    }

    bool verbose = false;
    if ((argc == 4) && (std::string(argv[3]) == "verbose")) {
        verbose = true;
    }

    try {
        modelsLoader_t modelsLoader(argv[1]);
        samplesLoader_t samplesLoader(argv[2]);

        std::multiset<modelStat_t> modelStats;
        for (const auto &i:modelsLoader.modelFiles()) {
            bool withDigits = true;
            if ((i.length() > 7) && (i.substr(i.length() - 7) == "_nd.w2v")) {
                withDigits = false;
            }
            mdlTester_t mdlTester(i);
            modelStat_t modelStat(i);
            if (verbose) {
                std::cout << "Testing " << i << ", threshold " << modelStat.threshold
                          << (withDigits?"":", digits are skipped") << std::endl;
            }
            for (const auto &j:samplesLoader.positivePairs()) {
                auto f = mdlTester.distance(j.first, j.second, withDigits);
                if (f < modelStat.minPositive) {
                    modelStat.minPositive = f;
                }
                modelStat.avgPositive += f;
            }
            modelStat.avgPositive /= samplesLoader.positivePairs().size();
            for (const auto &j:samplesLoader.negativePairs()) {
                auto f = mdlTester.distance(j.first, j.second, withDigits);
                if (f > modelStat.maxNegative) {
                    modelStat.maxNegative = f;
                }
                modelStat.avgNegative += f;
            }
            modelStat.avgNegative /= samplesLoader.negativePairs().size();

//            modelStat.threshold = (modelStat.minPositive + modelStat.maxNegative) / 2.0f;
            modelStat.threshold = (modelStat.avgNegative + modelStat.avgPositive) / 2.0f;

            for (const auto &j:samplesLoader.positivePairs()) {
                auto f = mdlTester.distance(j.first, j.second, withDigits);
                if (f < modelStat.threshold) {
                    modelStat.falseNegative++;
                    if (verbose) {
                        std::cout << "\tFalse negative, f = " << f
                                  << ", samples: " << j.first << " VS " << j.second << std::endl;
                    }
                } else {
                    if (verbose) {
                        std::cout << "\tPositive, f = " << f << std::endl;
                    }
                }
            }
            for (const auto &j:samplesLoader.negativePairs()) {
                auto f = mdlTester.distance(j.first, j.second, withDigits);
                if (f >= modelStat.threshold) {
                    modelStat.falsePositive++;
                    if (verbose) {
                        std::cout << "\tFalse positive, f = " << f
                                  << ", samples: " << j.first << " VS " << j.second << std::endl;
                    }
                } else {
                    if (verbose) {
                        std::cout << "\tNegative, f = " << f << std::endl;
                    }
                }
            }

            float truePositive = samplesLoader.positivePairs().size() - modelStat.falsePositive;
            float trueNegative = samplesLoader.negativePairs().size() - modelStat.falseNegative;
            modelStat.precision = truePositive / (truePositive + modelStat.falsePositive);
            modelStat.recall = truePositive / (truePositive + modelStat.falseNegative);
            modelStat.F1Score = 2.0f
                                * modelStat.precision * modelStat.recall
                                / (modelStat.precision + modelStat.recall);
            modelStat.accuracy = (truePositive + trueNegative)
                                 / (truePositive + trueNegative + modelStat.falsePositive + modelStat.falseNegative);

            modelStats.insert(modelStat);
        }
        std::size_t cnt = 0;
        std::cout << "Models raiting:" << std::endl;
        for (auto i = modelStats.crbegin(); i != modelStats.crend(); ++i) {
            std::cout << "#" << ++cnt << ": " << i->modelName << std::endl;
            std::cout << "\tsuggested threshold: " << i->threshold << std::endl;
            std::cout << "\tfalse positives: " << i->falsePositive << std::endl;
            std::cout << "\tfalse negatives: " << i->falseNegative << std::endl;
            std::cout << "\tavg positive: " << i->avgPositive << std::endl;
            std::cout << "\tavg negative: " << i->avgNegative << std::endl;
            std::cout << "\tmin positive: " << i->minPositive << std::endl;
            std::cout << "\tmax negative: " << i->maxNegative << std::endl;
            std::cout << "\tprecision: " << i->precision << std::endl;
            std::cout << "\trecall: " << i->recall << std::endl;
            std::cout << "\tF1 score: " << i->F1Score << std::endl;
            std::cout << "\taccuracy: " << i->accuracy << std::endl;
        }

        return EXIT_SUCCESS;
    } catch (const std::exception &_e) {
        std::cerr << _e.what() << std::endl;
    } catch (...) {
        std::cerr << "unknown error" << std::endl;
    }

    return EXIT_FAILURE;
}
