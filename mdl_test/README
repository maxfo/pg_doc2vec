mdl_test utility for word2vec models testing.

How it works.
Run mdl_test utility:
./mdl_test path_to_models_folder path_to_samples_folder verbose
where
    path_to_models_folder - path with word2vec models to test
    path_to_samples_folder - path to positive/negative samples folder
    verbose - optional parameter, specify it for more output information

1. The utility scans path_to_models_folder for word2vec models
2. The utility scans path_to_samples_folder for sample document pairs in the predefined subfolders - positive and
negative.
path_to_samples_folder structure:
    path_to_samples_folder -|
                            - positive |
                            |          - folder1 |
                            |          |         - file1
                            |          |         - file2
                            |          - folder2 |
                            |          |         - file1
                            |          |         - file2
                            |          ...
                            |          - folderN |
                            |                     - file1
                            |                     - file2
                            - negative |
                                       - folder1 |
                                       |         - file1
                                       |         - file2
                                       - folder2 |
                                       |         - file1
                                       |         - file2
                                       ...
                                       - folderN |
                                                 - file1
                                                 - file2
3. For each model, vector distances are calculated for positive and negative samples. Then the following model features
are calculated:
    - average positive distance
    - average negative distance
    - min positive distance
    - max negative distance
    - suggested threshold (min positive distance + max negative distance / 2), model's threshold between positive and
    negative decisions.
4. For each model, false positive/negative decisions are calculated based on suggested threshold (step 3)
5. The utility outputs final models rating, from the best models to the worst one
6. Output example:
Models raiting:
#1: /Users/macbookpro/Documents/devel/martin/models//cb_ns_500_10.w2v
	suggested threshold: 0.861402
	false positives: 0
	false negatives: 0
	avg positive: 0.928621
	avg negative: 0.708864
	min positive: 0.909011
	max negative: 0.813793
#2: /Users/macbookpro/Documents/devel/martin/models//cb_hs_500_10.w2v
	suggested threshold: 0.854656
	false positives: 0
	false negatives: 0
	avg positive: 0.905383
	avg negative: 0.731678
	min positive: 0.89765
	max negative: 0.811662
#3: /Users/macbookpro/Documents/devel/martin/models//sg_hs_500_10.w2v
	suggested threshold: 0.958136
	false positives: 0
	false negatives: 0
	avg positive: 0.978659
	avg negative: 0.924663
	min positive: 0.975647
	max negative: 0.940625
#4: /Users/macbookpro/Documents/devel/martin/models//sg_ns_500_10.w2v
	suggested threshold: 0.965793
	false positives: 0
	false negatives: 0
	avg positive: 0.982836
	avg negative: 0.937023
	min positive: 0.979255
	max negative: 0.952331
