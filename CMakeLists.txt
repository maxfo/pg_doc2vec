cmake_minimum_required(VERSION 3.7)
project(pg_doc2vec)

set(PROJECT_ROOT_DIR ${CMAKE_SOURCE_DIR})
set(CMAKE_BINARY_DIR ${PROJECT_ROOT_DIR}/bin)
set(EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR})
set(LIBRARY_OUTPUT_PATH ${CMAKE_BINARY_DIR}/lib)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_FLAGS "-Wall -Wextra -Wpedantic -Werror -fPIC")
set(CMAKE_CXX_FLAGS_DEBUG "-O0 -ggdb")
set(CMAKE_CXX_FLAGS_RELEASE "-Ofast -funroll-loops -ftree-vectorize -fPIC -DNDEBUG")

if (${CMAKE_SYSTEM_NAME} MATCHES "Linux" OR ${CMAKE_SYSTEM_NAME} MATCHES "FreeBSD")
    set(LIBS "-pthread")
endif()

if (${CMAKE_SYSTEM_NAME} MATCHES "Linux")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -s")
endif()

find_path(LIBEVENT_INCLUDE_DIR event2/buffer.h
        /usr/local/include
        /usr/include
        )
include_directories(${LIBEVENT_INCLUDE_DIR})
find_library(LIB_LIBEVENT_CORE event_core PATHS /usr/local/lib /usr/lib)
find_library(LIB_LIBEVENT_PTHREADS event_pthreads PATHS /usr/local/lib /usr/lib)

find_path(WORD2VEC_INCLUDE_DIR word2vec.h
        /usr/local/include
        /usr/include
        )
include_directories(${WORD2VEC_INCLUDE_DIR})
find_library(LIB_WORD2VEC word2vec PATHS /usr/local/lib /usr/lib)

include(FindProtobuf)
find_package(Protobuf REQUIRED)
add_subdirectory(protobuf)

set(LOCAL_INCLUDE_DIR ${PROJECT_ROOT_DIR})
include_directories(${LOCAL_INCLUDE_DIR})

add_subdirectory(logger)
include_directories(${LOCAL_INCLUDE_DIR}/logger)

add_subdirectory(fmt EXCLUDE_FROM_ALL)

set(WORKER_FILES
        worker/repository.h
        worker/repositoryThreads.cpp
        worker/repositoryThreads.h
        worker/repository.cpp
        worker/listener.h
        worker/listener.cpp
        worker/processor.h
        worker/processor.cpp
        worker/server.h
        worker/server.cpp
        )
add_library(d2v_server SHARED ${WORKER_FILES})
target_link_libraries(d2v_server
        ${LIB_LIBEVENT_CORE}
        ${LIB_LIBEVENT_PTHREADS}
        ${PROTOBUF_LIBRARIES}
        ${LIB_WORD2VEC}
        ${LIBS}
        d2v_proto logger fmt::fmt-header-only
        )

set(CLIENT_FILES
        client/d2v_client.h
        client/d2v_client.cpp
        )
add_library(d2v_client SHARED ${CLIENT_FILES})
target_link_libraries(d2v_client
        ${LIB_LIBEVENT_CORE}
        ${LIB_LIBEVENT_PTHREADS}
        ${PROTOBUF_LIBRARIES}
        ${LIBS}
        d2v_proto
        )

set(CLIENT_TEST_FILES
        tests/test_client.cpp
        )
add_executable(d2v_client_test ${CLIENT_TEST_FILES})
target_link_libraries(d2v_client_test d2v_client)

set(SERVER_TEST_FILES
        tests/test_server.cpp
        )
add_executable(d2v_server_test ${SERVER_TEST_FILES})
target_link_libraries(d2v_server_test d2v_server)

add_subdirectory(mdl_test)

install(TARGETS d2v_server DESTINATION lib)
install(TARGETS d2v_client DESTINATION lib)
