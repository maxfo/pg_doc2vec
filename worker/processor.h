//
// Created by Max Fomichev on 27/04/2017.
//

#ifndef PG_DOC2VEC_PROCESSOR_H
#define PG_DOC2VEC_PROCESSOR_H

#include <mutex>
#include <condition_variable>
#include <thread>
#include <queue>
#include <set>

#include <event2/bufferevent.h>

#include "logger.h"
#include "server.h"

class processor_t {
private:
    std::mutex m_mtxWrk;
    std::condition_variable m_cv;
    bool m_checkFlag = false;
    bool m_stopFlag = false;
    logger_t &m_logger;
    server_t m_server;
    std::thread m_workerThread;
    std::queue<struct bufferevent *> m_newClients;
    std::set<struct bufferevent *> m_clients;
    std::mutex m_mtxClnt;
    const uint8_t m_nwTimeout = 30;

public:
    processor_t(const std::string &_repPath, logger_t &_logger);
    ~processor_t();

    processor_t(const processor_t &) = delete;
    void operator=(const processor_t &) = delete;
    processor_t(const processor_t &&) = delete;
    void operator=(const processor_t &&) = delete;

    void newClient(struct bufferevent *_bev);

private:
    void worker();
};

#endif //PG_DOC2VEC_PROCESSOR_H
