//
// Created by Max Fomichev on 26/07/2017.
//

#include "repositoryThreads.h"

repositoryThreads_t::repositoryThreads_t(const std::string &_name,
                                         repository_t &_repository,
                                         logger_t &_logger): m_stopFlag(false), m_name(_name),
                                                             m_repository(_repository),
                                                             m_logger(_logger), m_txtHash(), m_gen(m_rd()) {
    for (uint8_t i = 0; i < 4; ++i) {
        m_threads.emplace_back(std::thread(&repositoryThreads_t::worker, this));
    }
}

repositoryThreads_t::~repositoryThreads_t() {
    {
        std::unique_lock<std::mutex> lck(m_mtxWrk);
        m_checkFlag = true;
        m_stopFlag = true;
        m_cvWrk.notify_all();
    }
    {
        std::unique_lock<std::mutex> lck(m_mtxRes);
        m_resultFlag = true;
        m_stopFlag = true;
        m_cvRes.notify_all();
    }
    for (auto &i:m_threads) {
        i.join();
    }

}

bool repositoryThreads_t::set(std::size_t _id, const std::string &_text) {
    {
        std::unique_lock<std::mutex> lck(m_mtxWrk);
        m_workQueue.emplace(queueRec_t(queueRec_t::recType_t::SET, _id, _text));
        m_checkFlag = true;
        m_cvWrk.notify_all();
    }

    while (true) {
        std::unique_lock<std::mutex> lck(m_mtxRes);
        while (!m_resultFlag) {
            if (m_cvRes.wait_for(lck, std::chrono::milliseconds(10)) == std::cv_status::timeout) {
                break;
            }
        }
        if (m_stopFlag) {
            return false;
        }
        const auto &i = m_resultMap.find(_id);
        if (i == m_resultMap.end()) {
            continue;
        }
        m_resultFlag = false;
        auto ret = i->second;
        m_resultMap.erase(i);
        m_logger.log(logger_t::LL_DEBUG, "repositoryThread: set completed");
        return ret.result;
    }
}

bool repositoryThreads_t::erase(std::size_t _id) {
    {
        std::unique_lock<std::mutex> lck(m_mtxWrk);
        m_workQueue.emplace(queueRec_t(queueRec_t::recType_t::ERASE, _id));
        m_checkFlag = true;
        m_cvWrk.notify_all();
    }

    while (true) {
        std::unique_lock<std::mutex> lck(m_mtxRes);
        while (!m_resultFlag) {
            if (m_cvRes.wait_for(lck, std::chrono::milliseconds(10)) == std::cv_status::timeout) {
                break;
            }
        }
        if (m_stopFlag) {
            return false;
        }
        const auto &i = m_resultMap.find(_id);
        if (i == m_resultMap.end()) {
            continue;
        }
        m_resultFlag = false;
        auto ret = i->second;
        m_resultMap.erase(i);
        m_logger.log(logger_t::LL_DEBUG, "repositoryThread: erase completed");
        return ret.result;
    }
}

bool repositoryThreads_t::nearest(std::size_t _id, std::size_t _limit,
                                  std::vector<std::pair<std::size_t, float>> &_nearest) {
    {
        std::unique_lock<std::mutex> lck(m_mtxWrk);
        m_workQueue.emplace(queueRec_t(_id, _limit));
        m_checkFlag = true;
        m_cvWrk.notify_all();
    }

    while (true) {
        std::unique_lock<std::mutex> lck(m_mtxRes);
        while (!m_resultFlag) {
            if (m_cvRes.wait_for(lck, std::chrono::milliseconds(10)) == std::cv_status::timeout) {
                break;
            }
        }
        if (m_stopFlag) {
            return false;
        }
        const auto &i = m_resultMap.find(_id);
        if (i == m_resultMap.end()) {
            continue;
        }
        m_resultFlag = false;
        auto ret = i->second;
        m_resultMap.erase(i);
        _nearest = std::move(ret.nearest);
        m_logger.log(logger_t::LL_DEBUG, "repositoryThread: nearest completed");
        return ret.result;
    }
}

bool repositoryThreads_t::nearest(const std::string &_text, std::size_t _limit,
                                  std::vector<std::pair<std::size_t, float>> &_nearest) {
    {
        std::unique_lock<std::mutex> lck(m_mtxWrk);
        m_workQueue.emplace(queueRec_t(_text, _limit));
        m_checkFlag = true;
        m_cvWrk.notify_all();
    }

    while (true) {
        std::unique_lock<std::mutex> lck(m_mtxRes);
        while (!m_resultFlag) {
            if (m_cvRes.wait_for(lck, std::chrono::milliseconds(10)) == std::cv_status::timeout) {
                break;
            }
        }
        if (m_stopFlag) {
            return false;
        }
        const auto &i = m_resultMap.find(m_txtHash(_text));
        if (i == m_resultMap.end()) {
            continue;
        }
        m_resultFlag = false;
        auto ret = i->second;
        m_resultMap.erase(i);
        _nearest = std::move(ret.nearest);
        m_logger.log(logger_t::LL_DEBUG, "repositoryThread: nearest completed");
        return ret.result;
    }
}

bool repositoryThreads_t::topRelated(std::size_t _limit, float _minDistance,
                std::vector<std::pair<std::size_t, std::size_t>> &_top) {
    auto id = m_dis(m_gen);
    {
        std::unique_lock<std::mutex> lck(m_mtxWrk);
        m_workQueue.emplace(queueRec_t(id, _limit, _minDistance));
        m_checkFlag = true;
        m_cvWrk.notify_all();
    }

    while (true) {
        std::unique_lock<std::mutex> lck(m_mtxRes);
        while (!m_resultFlag) {
            if (m_cvRes.wait_for(lck, std::chrono::milliseconds(10)) == std::cv_status::timeout) {
                break;
            }
        }
        if (m_stopFlag) {
            return false;
        }

        const auto &i = m_resultMap.find(id);
        if (i == m_resultMap.end()) {
            continue;
        }
        m_resultFlag = false;
        auto ret = i->second;
        m_resultMap.erase(i);
        _top = std::move(ret.top);
        m_logger.log(logger_t::LL_DEBUG, "repositoryThread: topRelated completed");
        return ret.result;
    }
}

void repositoryThreads_t::worker() noexcept {
    m_logger.log(logger_t::LL_DEBUG, "repositoryThread: started");
    m_ttl = m_repository.ttl(m_name);
    while (true) {
        queueRec_t i;
        bool hasRec = false;
        {
            std::unique_lock<std::mutex> lck(m_mtxWrk);
            while (!m_checkFlag) {
//                m_cvWrk.wait(lck);
                if (m_cvWrk.wait_for(lck, std::chrono::seconds((m_ttl > 48)?(m_ttl / 24 / 2):12))
                    == std::cv_status::timeout) {
                    break;
                }

            }
            if (m_stopFlag) {
                break;
            }
            if (!m_workQueue.empty()) {
                m_logger.log(logger_t::LL_DEBUG, "repositoryThread: new action");
                try {
                    i = m_workQueue.front();
                    m_workQueue.pop();
                    hasRec = true;
                } catch (...) {
                    continue;
                }
            }
            if (std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now()
                                                                 - m_lastCleanup).count() >= m_ttl / 24) {
                m_logger.log(logger_t::LL_DEBUG, "repositoryThread: worker cleanup processing");
                m_repository.cleanup(m_name);
                m_lastCleanup = std::chrono::system_clock::now();
            }
            if (m_workQueue.empty()) {
                m_checkFlag = false;
            }
            if (!hasRec) {
                continue;
            }
        }

        bool ret = true;
        std::vector<std::pair<std::size_t, float>> nearest;
        std::vector<std::pair<std::size_t, std::size_t>> top;
        try {
            switch (i.recType) {
                case queueRec_t::recType_t::SET: {
                    m_logger.log(logger_t::LL_DEBUG, "repositoryThread: worker set processing");
                    m_repository.set(m_name, i.id, i.text);
                    break;
                }
                case queueRec_t::recType_t::ERASE: {
                    m_logger.log(logger_t::LL_DEBUG, "repositoryThread: worker erase processing");
                    m_repository.erase(m_name, i.id);
                    break;
                }
                case queueRec_t::recType_t::NEAREST: {
                    if (i.text.empty()) {
                        m_logger.log(logger_t::LL_DEBUG, "repositoryThread: worker nearest by ID processing");
                        m_repository.nearest(m_name, i.id, i.limit, nearest);
                    } else {
                        m_logger.log(logger_t::LL_DEBUG, "repositoryThread: worker nearest by text processing");
                        m_repository.nearest(m_name, i.text, i.limit, nearest);
                    }
                    break;
                }
                case queueRec_t::recType_t::TOP_RELATED: {
                    m_logger.log(logger_t::LL_DEBUG, "repositoryThread: worker top relations processing");
                    m_repository.topRelated(m_name, i.limit, i.distance, top);
                    m_logger.log(logger_t::LL_DEBUG, "repositoryThread: worker top relations processed");
                    break;
                }
                default: {
                    m_logger.log(logger_t::LL_ERROR, "repositoryThread: unknown record type");
                }
            }
        } catch (const std::exception &_e) {
            m_logger.log(logger_t::LL_DEBUG, "repositoryThread: worker failed, {:s}", _e.what());
            ret = false;
        } catch (...) {
            m_logger.log(logger_t::LL_DEBUG, "repositoryThread: worker failed");
            ret = false;
        }
        {
            std::unique_lock<std::mutex> lck(m_mtxRes);
            if ((i.recType == queueRec_t::recType_t::NEAREST) && (!i.text.empty())) {
                m_resultMap[m_txtHash(i.text)].result = ret;
                m_resultMap[m_txtHash(i.text)].nearest = std::move(nearest);
                m_resultMap[m_txtHash(i.text)].top = std::move(top);
            } else {
                m_resultMap[i.id].result = ret;
                m_resultMap[i.id].nearest = std::move(nearest);
                m_resultMap[i.id].top = std::move(top);
            }
            m_resultFlag = true;
            m_cvRes.notify_all();
        }
        m_logger.log(logger_t::LL_DEBUG, "repositoryThread: worker completed");
    }
    m_logger.log(logger_t::LL_DEBUG, "repositoryThread: stopped");
}
