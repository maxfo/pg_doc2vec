//
//  server.h
//
//  Created by Max Fomichev on 16/11/2016.
//  Copyright © 2016 Pieci Kvadrati. All rights reserved.
//

#ifndef listener_h
#define listener_h

#include <thread>
#include <vector>

#include <event2/bufferevent.h>

#include "logger.h"
#include "processor.h"

class listener_t final {
private:
    struct evconnlistener *m_listener = nullptr;
    struct event_base *m_base = nullptr;
    std::string m_sockName;
    std::thread m_workerThread;
    logger_t &m_logger;
    std::unique_ptr<processor_t> m_processor;

public:
    listener_t(std::string _sockName, const std::string &_repPath, logger_t &_logger);
    ~listener_t() noexcept;

    listener_t(const listener_t &) = delete;
    void operator=(const listener_t &) = delete;
    listener_t(const listener_t &&) = delete;
    void operator=(const listener_t &&) = delete;

private:
    void worker() noexcept;
};

#endif /* listener_h */
