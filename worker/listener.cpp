//
//  server.cpp
//  libproxyproto / proxyd v3.0
//
//  Created by Max Fomichev on 16/11/2016.
//  Copyright © 2016 Pieci Kvadrati. All rights reserved.
//

#include <arpa/inet.h>
#include <sys/un.h>
#include <unistd.h>

#include <string>
//#include <cstring>

#include <event2/listener.h>
#include <event2/thread.h>
#include <event2/buffer.h>

#include "listener.h"

//static const char[] pg_doc2vecSocketName = "/tmp/pg_doc2vec.socket";

listener_t::listener_t(std::string _sockName,
                       const std::string &_repPath,
                       logger_t &_logger): m_sockName(std::move(_sockName)), m_logger(_logger),
                                           m_processor(new processor_t(_repPath, m_logger)) {
    if ((unlink(m_sockName.c_str()) != 0) && (errno != ENOENT)) {
        throw std::runtime_error("listener: libevent socket is busy");
    }

    evthread_use_pthreads();

    m_base = event_base_new();
    if (m_base == nullptr) {
        throw std::runtime_error("listener: libevent init failed");
    }

    struct sockaddr_un sun = {};
    std::memset(&sun, 0, sizeof(sun));
    sun.sun_family = AF_LOCAL;
    std::memcpy(sun.sun_path, m_sockName.data(), m_sockName.length());

    m_listener =
            evconnlistener_new_bind(m_base,
                                    [] (struct evconnlistener *_listener, evutil_socket_t _fd,
                                        struct sockaddr *, int , void *_ctx) {
                                        auto ec = static_cast<listener_t *>(_ctx);
                                        try {
                                            struct event_base *base = evconnlistener_get_base(_listener);
                                            struct ::bufferevent *bev =
                                                    bufferevent_socket_new(base,
                                                                           _fd,
                                                                           BEV_OPT_CLOSE_ON_FREE
                                                                           | BEV_OPT_THREADSAFE);
                                            ec->m_logger.log(logger_t::LL_DEBUG, "listener: new client");
                                            ec->m_processor->newClient(bev);
                                        } catch (...) {
                                            ec->m_logger.log(logger_t::LL_ERROR, "listener: new client failed");
                                        }
                                    },
                                    this, LEV_OPT_CLOSE_ON_FREE | LEV_OPT_REUSEABLE | LEV_OPT_THREADSAFE, -1,
                                    (struct sockaddr*) &sun, sizeof(sun));

    if (m_listener == nullptr) {
        m_logger.log(logger_t::LL_DEBUG, "listener: listener create failed");
        throw std::runtime_error("listener create failed");
    }

    evconnlistener_set_error_cb(m_listener,
//                                [] (struct evconnlistener *_listener, void *_ctx) {
                                [] (struct evconnlistener *, void *_ctx) {
                                    try {
                                        auto ec = static_cast<listener_t *>(_ctx);
                                        ec->m_logger.log(logger_t::LL_ERROR,
                                                           "listener: listen error %d",
                                                           EVUTIL_SOCKET_ERROR());

                                    } catch(...) {}
                                });

    m_workerThread = std::thread(&listener_t::worker, this);
}

listener_t::~listener_t() noexcept {
    event_base_loopexit(m_base, nullptr);
    m_workerThread.join();
    if (m_listener != nullptr) {
        evconnlistener_free(m_listener);
    }
    event_base_free(m_base);
}

void listener_t::worker() noexcept {
    m_logger.log(logger_t::LL_DEBUG, "listener: started");
    event_base_dispatch(m_base);
    m_logger.log(logger_t::LL_DEBUG, "listener: stopped");
}
