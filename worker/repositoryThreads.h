//
// Created by Max Fomichev on 26/07/2017.
//

#ifndef PG_DOC2VEC_REPOSITORYTHREADS_H
#define PG_DOC2VEC_REPOSITORYTHREADS_H

#include <string>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <unordered_map>
#include <queue>
#include <atomic>
#include <random>

#include "logger.h"
#include "repository.h"

class repositoryThreads_t {
public:
    struct queueRec_t {
        enum class recType_t {
            UNKN,
            SET,
            ERASE,
            NEAREST,
            TOP_RELATED
        };
        recType_t recType = recType_t::UNKN;
        std::size_t id = 0;
        std::string text;
        std::size_t limit = 0;
        float distance = 0.0f;

        queueRec_t() = default;
        queueRec_t(recType_t _recType, std::size_t _id, std::string _text = ""): recType(_recType),
                                                                                 id(_id),
                                                                                 text(std::move(_text)) {}
        queueRec_t(std::string _text, std::size_t _limit): recType(recType_t::NEAREST),
                                                           text(std::move(_text)),
                                                           limit(_limit) {}
        explicit queueRec_t(std::size_t _id, std::size_t _limit): recType(recType_t::NEAREST),
                                                                  id(_id),
                                                                  limit(_limit) {}
        explicit queueRec_t(std::size_t _id, std::size_t _limit, float _distance): recType(recType_t::TOP_RELATED),
                                                                                   id(_id),
                                                                                   limit(_limit),
                                                                                   distance(_distance) {}
    };

    struct resultRec_t {
        bool result = false;
        std::vector<std::pair<std::size_t, float>> nearest;
        std::vector<std::pair<std::size_t, std::size_t>> top;
    };

private:
    std::vector<std::thread> m_threads;
    std::mutex m_mtxWrk;
    std::mutex m_mtxRes;
    std::condition_variable m_cvWrk;
    std::condition_variable m_cvRes;
    bool m_checkFlag = false;
    bool m_resultFlag = false;
    std::atomic<bool> m_stopFlag;

    std::string m_name;
    repository_t &m_repository;
    logger_t &m_logger;
    std::queue<queueRec_t> m_workQueue;
    std::unordered_map<std::size_t, resultRec_t> m_resultMap;
    std::hash<std::string> m_txtHash;

    std::chrono::system_clock::time_point m_lastCleanup {std::chrono::system_clock::now()};
    uint32_t m_ttl = 0;

    std::random_device m_rd;
    std::mt19937 m_gen;
    std::uniform_int_distribution<std::size_t> m_dis;

public:
    repositoryThreads_t(const std::string &_name, repository_t &_repository, logger_t &_logger);
    ~repositoryThreads_t();
    repositoryThreads_t(const repositoryThreads_t &) = delete;
    void operator=(const repositoryThreads_t &) = delete;
    repositoryThreads_t(const repositoryThreads_t &&) = delete;
    void operator=(const repositoryThreads_t &&) = delete;

    bool set(std::size_t _id, const std::string &_text);
    bool erase(std::size_t _id);
    bool nearest(std::size_t _id, std::size_t _limit,
                 std::vector<std::pair<std::size_t, float>> &_nearest);
    bool nearest(const std::string &_text, std::size_t _limit,
                 std::vector<std::pair<std::size_t, float>> &_nearest);
    bool topRelated(std::size_t _limit, float _minDistance,
                    std::vector<std::pair<std::size_t, std::size_t>> &_top);

private:
    void worker() noexcept;
};

#endif //PG_DOC2VEC_REPOSITORYTHREADS_H
