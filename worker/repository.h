//
// Created by Max Fomichev on 10/05/2017.
//

#ifndef PG_DOC2VEC_REPOSITORY_H
#define PG_DOC2VEC_REPOSITORY_H

#include <string>
#include <unordered_map>
#include <vector>
#include <shared_mutex>
#include <chrono>

#include <word2vec.h>

#include "logger.h"

class repository_t {
private:
    struct d2vModelData_t {
        uint32_t ttl;
        std::string fileName;
        w2v::d2vModel_t d2vModel;
        std::unordered_map<std::size_t, std::chrono::system_clock::time_point> docTTLs;
        mutable std::shared_mutex mtx;

        d2vModelData_t(uint32_t _ttl, std::string _fileName, uint16_t _vectorSize):
                ttl(_ttl), fileName(std::move(_fileName)), d2vModel(_vectorSize), docTTLs() {
            d2vModel.load(fileName);
        }
        ~d2vModelData_t() = default;
        d2vModelData_t(const d2vModelData_t &) = delete;
        void operator=(const d2vModelData_t &) = delete;
        d2vModelData_t(const d2vModelData_t &&) = delete;
        void operator=(const d2vModelData_t &&) = delete;
    };

    logger_t *m_logger = nullptr;
    std::string m_repositoryPath;
    std::string m_w2vFileName;
    std::unique_ptr<w2v::w2vModel_t> m_w2vModel;
    std::string m_eow;
    std::unordered_map<std::string, std::unique_ptr<d2vModelData_t>> m_d2vModels;
    mutable std::shared_mutex m_mtx;
    bool m_inited = false;

    const std::string repositoryFileName = "/d2v.rep";

public:
    static repository_t &repository() {
        static repository_t repository;
        return repository;
    }
    ~repository_t();

    repository_t(const repository_t &) = delete;
    void operator=(const repository_t &) = delete;
    repository_t(const repository_t &&) = delete;
    void operator=(const repository_t &&) = delete;

    void init(const std::string &_path, logger_t &_logger, std::vector<std::string> &_repNames);

    uint32_t ttl(const std::string &_name) const;

    void create(const std::string &_name, uint32_t _ttl);
    void drop(const std::string &_name);
    void cleanup(const std::string &_name);

    void set(const std::string &_name, std::size_t _id, const std::string &_text);
    void erase(const std::string &_name, std::size_t _id);

    void nearest(const std::string &_name, std::size_t _id, std::size_t _limit,
                 std::vector<std::pair<std::size_t, float>> &_nearest);
    void nearest(const std::string &_name, const std::string &_text, std::size_t _limit,
                 std::vector<std::pair<std::size_t, float>> &_nearest);
    void topRelated(const std::string &_name, std::size_t _limit, float _minDistance,
                    std::vector<std::pair<std::size_t, std::size_t>> &_top);

private:
    repository_t() = default;
};


#endif //PG_DOC2VEC_REPOSITORY_H
