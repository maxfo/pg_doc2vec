//
// Created by Max Fomichev on 27/04/2017.
//

#include <event2/event.h>
#include <event2/buffer.h>

#include "protobuf/server.h"
#include "processor.h"

processor_t::processor_t(const std::string &_repPath, logger_t &_logger): m_logger(_logger),
                                                                          m_server(_repPath, _logger) {
    m_workerThread = std::thread(&processor_t::worker, this);
}

processor_t::~processor_t() {
    {
        std::unique_lock<std::mutex> lck(m_mtxWrk);
        m_checkFlag = true;
        m_stopFlag = true;
        m_cv.notify_one();
    }
    m_workerThread.join();

    try {
        std::unique_lock<std::mutex> lck(m_mtxClnt);
        auto i = m_clients.begin();
        while (i != m_clients.end()) {
            bufferevent_free(*i);
            i = m_clients.erase(i);
        }
    } catch(...) {}
}

void processor_t::newClient(struct bufferevent *_bev) {
    std::unique_lock<std::mutex> lck(m_mtxWrk);
    m_newClients.push(_bev);
    m_checkFlag = true;
    m_cv.notify_one();
}

void processor_t::worker() {
    m_logger.log(logger_t::LL_DEBUG, "processor: started");
    while (true) {
        struct bufferevent *bev = nullptr;
        {
            std::unique_lock<std::mutex> lck(m_mtxWrk);
            while (!m_checkFlag) {
                m_cv.wait(lck);
            }

            if (m_stopFlag) {
                break;
            }

            if (!m_newClients.empty()) {
                try {
                    bev = m_newClients.front();
                    m_newClients.pop();
                } catch(...) {
                    continue;
                }
            } else {
                m_checkFlag = false;
                continue;
            }
            if (m_newClients.empty()) {
                m_checkFlag = false;
            }
        }

        bufferevent_setcb(bev,
                          [] (struct bufferevent *_bev, void *_ctx) {
                              auto cntrl = static_cast<processor_t *>(_ctx);
                              try {
                                  cntrl->m_logger.log(logger_t::LL_DEBUG, "processor: read event");

                                  struct evbuffer *input = bufferevent_get_input(_bev);
                                  struct evbuffer *output = bufferevent_get_output(_bev);

                                  auto ebLen = evbuffer_get_length(input);
                                  std::vector<char> readBuf;
                                  readBuf.resize(ebLen + 1, 0);
                                  evbuffer_remove(input, readBuf.data(), ebLen);

                                  {
                                      std::unique_lock<std::mutex> lck(cntrl->m_mtxClnt);
                                      auto i = cntrl->m_clients.find(_bev);
                                      if (i == cntrl->m_clients.end()) {
                                          bufferevent_free(_bev);
                                          cntrl->m_logger.log(logger_t::LL_ERROR, "processor: unknown client");
                                          return;
                                      }
                                  }

                                  pb::server::request_t::serverCallbacks_t serverCallbacks;
                                  serverCallbacks.scbCreate = server_t::create;
                                  serverCallbacks.scbDrop = server_t::drop;
                                  serverCallbacks.scbSet = server_t::set;
                                  serverCallbacks.scbErase = server_t::erase;
                                  serverCallbacks.scbNearestID = server_t::nearestID;
                                  serverCallbacks.scbNearestText = server_t::nearestText;
                                  serverCallbacks.scbTopRelated = server_t::topRelated;
                                  std::vector<char> writeBuf;
                                  {
                                      pb::server::request_t request(serverCallbacks);
                                      request(readBuf, writeBuf, static_cast<void *>(&cntrl->m_server));
                                  }
                                  evbuffer_add(output, writeBuf.data(), writeBuf.size());

                                  return;
                              } catch (const std::exception &_e) {
                                  cntrl->m_logger.log(logger_t::LL_ERROR, "processor: %s", _e.what());
                              } catch (...) {
                                  cntrl->m_logger.log(logger_t::LL_ERROR, "processor: unknown error on red");
                              }
//                                      bufferevent_free(_bev);
                          }, nullptr,
                          [] (struct bufferevent *_bev, short _what, void *_ctx) {
                              try {
                                  auto cntrl = static_cast<processor_t *>(_ctx);
                                  if (cntrl == nullptr) {
                                      return;
                                  }

                                  if ((_what & (BEV_EVENT_TIMEOUT | BEV_EVENT_EOF | BEV_EVENT_ERROR)) != 0) {
                                      std::unique_lock<std::mutex> lck(cntrl->m_mtxClnt);
                                      auto i = cntrl->m_clients.find(_bev);
                                      if (i != cntrl->m_clients.end()) {
                                          cntrl->m_clients.erase(i);
                                      }
                                      bufferevent_free(_bev);
                                  }
                                  if ((_what & (BEV_EVENT_TIMEOUT)) != 0) {
                                      cntrl->m_logger.log(logger_t::LL_DEBUG, "processor: io timeout");
                                  }
                                  if ((_what & BEV_EVENT_ERROR) != 0) {
                                      cntrl->m_logger.log(logger_t::LL_DEBUG, "processor: io error");
                                  }
                                  if ((_what & BEV_EVENT_EOF) != 0) {
                                      cntrl->m_logger.log(logger_t::LL_DEBUG, "processor: closed socket");
                                  }
                              } catch (...) {}
                          },
                          this);

        struct timeval timeout = {m_nwTimeout, m_nwTimeout};
        bufferevent_set_timeouts(bev, &timeout, &timeout);
        std::unique_lock<std::mutex> lck(m_mtxClnt);
        m_clients.insert(bev);
        bufferevent_enable(bev, EV_READ | EV_WRITE);
    }
    m_logger.log(logger_t::LL_DEBUG, "processor: stopped");
}
