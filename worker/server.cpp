//
// Created by Max Fomichev on 24/07/2017.
//

#include "server.h"

server_t::server_t(const std::string &_repositoryPath, logger_t &_logger): m_logger(_logger),
                                                                           m_repository(repository_t::repository()) {
    m_logger.log(logger_t::LL_DEBUG, "server: open repository at {:s}", _repositoryPath);
    std::vector<std::string> repNames;
    m_repository.init(_repositoryPath, _logger, repNames);
    m_logger.log(logger_t::LL_DEBUG, "server: found {:d} repositories", repNames.size());

    for (const auto &i:repNames) {
        m_repositoryThreadsMap.emplace(i,
                                       std::unique_ptr<repositoryThreads_t>(new repositoryThreads_t(i,
                                                                                                    m_repository,
                                                                                                    m_logger)));
    }
}

bool server_t::create(const std::string &_name, uint32_t _ttl, void *_ctx) {
    try {
        auto ctx = static_cast<server_t *>(_ctx);
        ctx->m_repository.create(_name, _ttl);
        ctx->m_repositoryThreadsMap.emplace(_name,
                                              std::unique_ptr<repositoryThreads_t>(
                                                      new repositoryThreads_t(_name,
                                                                              ctx->m_repository,
                                                                              ctx->m_logger)));
    } catch (...) {
        return false;
    }

    return true;
}

bool server_t::drop(const std::string &_name, void *_ctx) {
    try {
        auto ctx = static_cast<server_t *>(_ctx);
        ctx->m_repository.drop(_name);
        ctx->m_repositoryThreadsMap.erase(_name);
    } catch (...) {
        return false;
    }

    return true;
}

bool server_t::set(const std::string &_name, std::size_t _id, const std::string &_text, void *_ctx) {
    try {
        auto ctx = static_cast<server_t *>(_ctx);
        auto &i = ctx->m_repositoryThreadsMap.at(_name);
        return i->set(_id, _text);
    } catch (...) {
        return false;
    }
}

bool server_t::erase(const std::string &_name, std::size_t _id, void *_ctx) {
    try {
        auto ctx = static_cast<server_t *>(_ctx);
        auto &i = ctx->m_repositoryThreadsMap.at(_name);
        return i->erase(_id);
    } catch (...) {
        return false;
    }
}

bool server_t::nearestID(const std::string &_name, std::size_t _id, std::size_t _limit,
                       std::vector<std::pair<std::size_t, float>> &_nearest, void *_ctx) {
    try {
        auto ctx = static_cast<server_t *>(_ctx);
        auto &i = ctx->m_repositoryThreadsMap.at(_name);
        return i->nearest(_id, _limit, _nearest);
    } catch (...) {
        return false;
    }
}

bool server_t::nearestText(const std::string &_name, const std::string &_text, std::size_t _limit,
                       std::vector<std::pair<std::size_t, float>> &_nearest, void *_ctx) {
    try {
        auto ctx = static_cast<server_t *>(_ctx);
        auto &i = ctx->m_repositoryThreadsMap.at(_name);
        return i->nearest(_text, _limit, _nearest);
    } catch (...) {
        return false;
    }
}

bool server_t::topRelated(const std::string &_name, std::size_t _limit, float _distance,
                       std::vector<std::pair<std::size_t, std::size_t>> &_top, void *_ctx) {
    try {
        auto ctx = static_cast<server_t *>(_ctx);
        auto &i = ctx->m_repositoryThreadsMap.at(_name);
        return i->topRelated(_limit, _distance, _top);
    } catch (...) {
        return false;
    }
}
