//
// Created by Max Fomichev on 24/07/2017.
//

#ifndef PG_DOC2VEC_WRK_SERVER_H
#define PG_DOC2VEC_WRK_SERVER_H

#include <vector>
//#include <thread>
//#include <random>
//#include <mutex>
//#include <condition_variable>
#include <unordered_map>

#include "logger.h"
#include "repository.h"
#include "repositoryThreads.h"

class server_t final {
private:
    logger_t &m_logger;
    std::unordered_map<std::string, std::unique_ptr<repositoryThreads_t>> m_repositoryThreadsMap;
    repository_t &m_repository;

public:
    server_t(const std::string &_repositoryPath, logger_t &_logger);
    ~server_t() = default;

    server_t(const server_t &) = delete;
    void operator=(const server_t &) = delete;
    server_t(const server_t &&) = delete;
    void operator=(const server_t &&) = delete;

    static bool create(const std::string &_name, uint32_t _ttl, void *_ctx);
    static bool drop(const std::string &_name, void *_ctx);
    static bool set(const std::string &_name, std::size_t _id, const std::string &_text, void *_ctx);
    static bool erase(const std::string &_name, std::size_t _id, void *_ctx);
    static bool nearestID(const std::string &_name, std::size_t _id, std::size_t _limit,
                        std::vector<std::pair<std::size_t, float>> &_nearest, void *_ctx);
    static bool nearestText(const std::string &_name, const std::string &_text, std::size_t _limit,
                        std::vector<std::pair<std::size_t, float>> &_nearest, void *_ctx);
    static bool topRelated(const std::string &_name, std::size_t _limit, float _distance,
                          std::vector<std::pair<std::size_t, std::size_t>> &_top, void *_ctx);
};

#endif //PG_DOC2VEC_WRK_SERVER_H
