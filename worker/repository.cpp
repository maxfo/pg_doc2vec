//
// Created by Max Fomichev on 10/05/2017.
//

#include <unistd.h>

#include <algorithm>
#include <fstream>

#include "repository.h"

repository_t::~repository_t() {
    if (!m_inited) {
        return;
    }

    // save repository
    std::ofstream ofs;
    ofs.open((m_repositoryPath + repositoryFileName).c_str(), std::ofstream::out | std::ofstream::trunc);
    if (ofs.fail()) {
        m_logger->log(logger_t::LL_ERROR,
                      "d2v repository file open failed (writing), {:s}",
                      m_repositoryPath + repositoryFileName);
        return;
    }
    ofs << "word2vec " << m_w2vFileName << std::endl;
    {
        std::unique_lock<std::shared_mutex> repLck(m_mtx);
        for (const auto &i:m_d2vModels) {
            // save doc2vec model
            i.second->d2vModel.save(i.second->fileName);
            // save model data
            ofs << i.first << " " << i.second->fileName << " " << i.second->ttl << std::endl;

            std::ofstream ttlOfs;
            ttlOfs.open((i.second->fileName + ".ttl").c_str(), std::ofstream::out
                                                               | std::ofstream::trunc
                                                               | std::ofstream::binary);
            if (ttlOfs.fail()) {
                m_logger->log(logger_t::LL_ERROR,
                              "d2v repository ttls file open failed (writing), {:s}",
                              i.second->fileName + ".ttl");
                return;
            }
            for (const auto &j:i.second->docTTLs) {
                auto t = std::chrono::system_clock::to_time_t(j.second);
                ttlOfs.write(reinterpret_cast<const char *>(&(j.first)), sizeof(j.first));
                ttlOfs.write(reinterpret_cast<const char *>(&t), sizeof(t));
            }
            ttlOfs.close();
        }
    }
    ofs.close();
    m_logger->log(logger_t::LL_DEBUG, "repository: deinitialised");
}

void repository_t::init(const std::string &_path, logger_t &_logger, std::vector<std::string> &_repNames) {
    m_logger = &_logger;
    // load repository
    m_repositoryPath = _path;
    std::unordered_map<std::string, std::pair<std::string, uint32_t>> d2vFiles;
    std::ifstream ifs;
    ifs.open((m_repositoryPath + repositoryFileName).c_str());
    if (ifs.fail()) {
        throw std::runtime_error(std::string("d2v repository file open failed (reading), ")
                                 + m_repositoryPath + repositoryFileName);
    }
    while (!ifs.eof()) {
        std::string name, path;
        uint32_t ttl;
        ifs >> name;
        if (name.empty()) {
            continue;
        }
        ifs >> path;
        if (path.empty()) {
            throw std::runtime_error("wrong repository file format (path value)");
        }
        if (name == "word2vec") {
            m_w2vFileName = path;
            continue;
        }
        ifs >> ttl;
        if (ttl < 24) {
            throw std::runtime_error("wrong repository file format (ttl value)");
        }
        d2vFiles[name] = std::make_pair(path, ttl);
    }
    ifs.close();

    // load wor2vec model
    m_w2vModel.reset(new w2v::w2vModel_t());
    if (!m_w2vModel->load(m_w2vFileName)) {
        throw std::runtime_error(m_w2vModel->errMsg());
    }

    // should we ignore digits?
    bool withDigits = true;
    if ((m_w2vFileName.length() > 7) && (m_w2vFileName.substr(m_w2vFileName.length() - 7) == "_nd.w2v")) {
        withDigits = false;
    }
    if (withDigits) {
        m_eow = " \n,.-!?:;/\"#$%&'()*+<=>@[]\\^_`{|}~\t\v\f\r";
    } else {
        m_eow = " \n,.-!?:;/\"#$%&'()*+<=>@[]\\^_`{|}~\t\v\f\r1234567890";
    }


    // load doc2vec models
    for (const auto &i:d2vFiles) {
        auto d = m_d2vModels.emplace(i.first,
                                     std::unique_ptr<d2vModelData_t>(new d2vModelData_t(i.second.second,
                                                                                        i.second.first,
                                                                                        m_w2vModel->vectorSize())));

        std::ifstream ttlIfs;
        ttlIfs.open((i.second.first + ".ttl").c_str(), std::ifstream::in | std::ifstream::binary);
        if (ttlIfs.fail()) {
            throw std::runtime_error("d2v repository ttls file open failed (reading), " + i.second.first + ".ttl");
        }
        while(true) {
            std::size_t id = 0;
            std::time_t t = 0;
            if (!ttlIfs.read(reinterpret_cast<char *>(&(id)), sizeof(id))
                || !ttlIfs.read(reinterpret_cast<char *>(&t), sizeof(t))) {
                break;
            }
            d.first->second->docTTLs.emplace(id, std::chrono::system_clock::from_time_t(t));
        }
        ttlIfs.close();

        _repNames.push_back(i.first);
    }

    m_inited = true;

    m_logger->log(logger_t::LL_DEBUG, "repository: initialised");
}

void repository_t::create(const std::string &_name, uint32_t _ttl) {
    if (!m_inited) {
        throw std::runtime_error("repository is uninitialised");
    }
    if (_ttl < 24) {
        throw std::runtime_error("wrong ttl value");
    }
    std::unique_lock<std::shared_mutex> repLck(m_mtx);
    if (m_d2vModels.find(_name) != m_d2vModels.end()) {
        throw std::runtime_error("d2v model already exists");
    }

    m_d2vModels.emplace(_name,
                        std::unique_ptr<d2vModelData_t>(new d2vModelData_t(_ttl,
                                                                           m_repositoryPath + "/" + _name + ".d2v",
                                                                           m_w2vModel->vectorSize())));
    m_logger->log(logger_t::LL_DEBUG, "repository: create completed");
}

void repository_t::drop(const std::string &_name) {
    if (!m_inited) {
        throw std::runtime_error("repository is uninitialised");
    }

    std::unique_lock<std::shared_mutex> repLck(m_mtx);
    const auto i = m_d2vModels.find(_name);
    if (i == m_d2vModels.end()) {
        throw std::runtime_error("no such d2v model");
    }

    remove(i->second->fileName.c_str());
    m_d2vModels.erase(i);
    m_logger->log(logger_t::LL_DEBUG, "repository: drop completed");
}

uint32_t repository_t::ttl(const std::string &_name) const {
    if (!m_inited) {
        throw std::runtime_error("repository is uninitialised");
    }

    std::shared_lock<std::shared_mutex> repLck(m_mtx);
    const auto i = m_d2vModels.find(_name);
    if (i == m_d2vModels.end()) {
        throw std::runtime_error("no such d2v model");
    }

    return i->second->ttl;
}

void repository_t::cleanup(const std::string &_name) {
    if (!m_inited) {
        throw std::runtime_error("repository is uninitialised");
    }

    std::shared_lock<std::shared_mutex> repLck(m_mtx);
    const auto i = m_d2vModels.find(_name);
    if (i == m_d2vModels.end()) {
        throw std::runtime_error("no such d2v model");
    }

    std::unique_lock<std::shared_mutex> d2wLck(i->second->mtx);
    auto j = i->second->docTTLs.begin();
    while (j != i->second->docTTLs.end()) {
        if (std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now()
                                                             - j->second).count() >= i->second->ttl) {
            m_logger->log(logger_t::LL_DEBUG, "repository: removing outdated id {:d}", j->first);
            i->second->d2vModel.erase(j->first);
            j = i->second->docTTLs.erase(j);
        } else {
            ++j;
        }
    }
    m_logger->log(logger_t::LL_DEBUG, "repository: cleanup completed");
}

void repository_t::set(const std::string &_name, std::size_t _id, const std::string &_text) {
    if (!m_inited) {
        throw std::runtime_error("repository is uninitialised");
    }

    std::shared_lock<std::shared_mutex> repLck(m_mtx);
    const auto i = m_d2vModels.find(_name);
    if (i == m_d2vModels.end()) {
        throw std::runtime_error("no such d2v model");
    }

    std::unique_lock<std::shared_mutex> d2wLck(i->second->mtx);
    i->second->d2vModel.set(_id, w2v::doc2vec_t(m_w2vModel, _text, m_eow), true);
    i->second->docTTLs[_id] = std::chrono::system_clock::now();
    m_logger->log(logger_t::LL_DEBUG, "repository: set completed");
}

void repository_t::erase(const std::string &_name, std::size_t _id) {
    if (!m_inited) {
        throw std::runtime_error("repository is uninitialised");
    }

    std::shared_lock<std::shared_mutex> repLck(m_mtx);
    const auto i = m_d2vModels.find(_name);
    if (i == m_d2vModels.end()) {
        throw std::runtime_error("no such d2v model");
    }

    std::unique_lock<std::shared_mutex> d2wLck(i->second->mtx);
    i->second->d2vModel.erase(_id);
    i->second->docTTLs.erase(_id);
    m_logger->log(logger_t::LL_DEBUG, "repository: erase completed");
}

void repository_t::nearest(const std::string &_name, std::size_t _id, std::size_t _limit,
             std::vector<std::pair<std::size_t, float>> &_nearest) {
    if (!m_inited) {
        throw std::runtime_error("repository is uninitialised");
    }

    std::shared_lock<std::shared_mutex> repLck(m_mtx);
    const auto i = m_d2vModels.find(_name);
    if (i == m_d2vModels.end()) {
        throw std::runtime_error("no such d2v model");
    }

    std::shared_lock<std::shared_mutex> d2wLck(i->second->mtx);
    auto v = i->second->d2vModel.vector(_id);
    if (v == nullptr) {
        throw std::runtime_error("no such id");
    }

    i->second->d2vModel.nearest(*v, _nearest, _limit);
    m_logger->log(logger_t::LL_DEBUG, "repository: nearest completed");
}

void repository_t::nearest(const std::string &_name, const std::string &_text, std::size_t _limit,
             std::vector<std::pair<std::size_t, float>> &_nearest) {
    if (!m_inited) {
        throw std::runtime_error("repository is uninitialised");
    }

    std::shared_lock<std::shared_mutex> repLck(m_mtx);
    const auto i = m_d2vModels.find(_name);
    if (i == m_d2vModels.end()) {
        throw std::runtime_error("no such d2v model");
    }

    std::shared_lock<std::shared_mutex> d2wLck(i->second->mtx);
    i->second->d2vModel.nearest(w2v::doc2vec_t(m_w2vModel, _text, m_eow), _nearest, _limit);
    m_logger->log(logger_t::LL_DEBUG, "repository: nearest completed");
}

void repository_t::topRelated(const std::string &_name, std::size_t _limit, float _minDistance,
                              std::vector<std::pair<std::size_t, std::size_t>> &_top) {
    if (!m_inited) {
        throw std::runtime_error("repository is uninitialised");
    }

    using relation_t = std::pair<std::size_t, float>; // related ID & distance
    using relationVector_t = std::vector<relation_t>; // vector of related IDs and their distances
    using relationRecord_t = std::pair<std::size_t, relationVector_t >; // id and its relations
    using relationMatrix_t = std::vector<relationRecord_t>; // relation matrix of all documents

    relationMatrix_t relationMatrix;

    {
        // lock repository for writing
        std::shared_lock<std::shared_mutex> repLck(m_mtx);
        // and get model
        const auto &model = m_d2vModels.find(_name);
        if (model == m_d2vModels.end()) {
            throw std::runtime_error("no such d2v model");
        }

        // lock model for writing
        std::shared_lock<std::shared_mutex> d2wLck(model->second->mtx);
        for (const auto i:model->second->docTTLs) {
            // get vector by id
            auto v = model->second->d2vModel.vector(i.first);
            if (v == nullptr) {
                continue;
            }
            // get nearest ids
            relationVector_t relationVector;
            model->second->d2vModel.nearest(*v, relationVector, model->second->d2vModel.modelSize(), _minDistance);
            // and save it in the relation matrix
            if (!relationVector.empty()) {
                relationMatrix.emplace_back(relationRecord_t(i.first, relationVector));
            }
        }
    }
    m_logger->log(logger_t::LL_DEBUG, "repository: topRelated - on enter, relationMatrix size = {:d}",
                  relationMatrix.size());

    // sort relation matrix, from id with max relations to id with min relations
    std::sort(relationMatrix.begin(), relationMatrix.end(),
              [](const relationRecord_t &_l, const relationRecord_t &_r) {
                  return _l.second.size() > _r.second.size();
              });

    // remove dublicated relations
    for (auto rmi = relationMatrix.begin(); ;) {
        if (rmi == relationMatrix.end()) {
            break;
        }
        relationMatrix.erase(std::remove_if(rmi + 1, relationMatrix.end(),
                                            [&rmi](const relationRecord_t &_rr) {
                                                for (const auto &i:rmi->second) {
                                                    if (i.first == _rr.first) {
                                                        return true;
                                                    }
                                                }
                                                return false;
                                            }), relationMatrix.end());

        for (auto rmj = rmi + 1; rmj != relationMatrix.end(); ++rmj) {
            rmj->second.erase(std::remove_if(rmj->second.begin(), rmj->second.end(),
                                             [&rmi](const relation_t &_r) {
                                                 for (const auto &rv:rmi->second) {
                                                     if (_r.first == rv.first) {
                                                         return true;
                                                     }
                                                 }
                                                 return false;
                                             }
            ), rmj->second.end());
        }

        ++rmi;
    }
    m_logger->log(logger_t::LL_DEBUG, "repository: topRelated - on exit, relationMatrix size = {:d}",
                  relationMatrix.size());

    // sort relation matrix again
    std::sort(relationMatrix.begin(), relationMatrix.end(),
              [](const relationRecord_t &_l, const relationRecord_t &_r) {
                  return _l.second.size() > _r.second.size();
              });

    // fill out result vector
    std::size_t cnt = 0;
    for (const auto &i:relationMatrix) {
        if (++cnt > _limit) {
            break;
        }
        _top.emplace_back(std::pair<std::size_t, std::size_t>(i.first, i.second.size()));
    }

    m_logger->log(logger_t::LL_DEBUG, "repository: topRelated completed");
}
