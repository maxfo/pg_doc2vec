//
//  logger.cpp
//  kamgr
//
//  Created by Max Fomichev on 07/10/2016.
//  Copyright © 2016 Pieci Kvadrati. All rights reserved.
//

#include <cstdio>
#include <cstdarg>
#include <syslog.h>

#include <cstring>
#include <iostream>

#include "logger.h"

logger_t::~logger_t() {
    if (m_logDst == LD_SYSLOG) {
        closelog();
    } else if (m_logDst == LD_FILE) {
        m_ofs.close();
    }
    
    {
        std::unique_lock<std::mutex> lck(m_mtxLog);
        m_checkQueueFlag = true;
        m_workFlag = false;
        m_cvLog.notify_one();
    }
    m_workerThread.join();
}

void logger_t::init(const std::string &_logPrefix, const std::string &_logTo, const std::string &_logLevel) {
    m_logPrefix = _logPrefix;
    if (_logLevel == "critical") {
        m_logLevel = LL_CRITICAL;
    } else if (_logLevel == "error") {
        m_logLevel = LL_ERROR;
    } else if (_logLevel == "warning") {
        m_logLevel = LL_WARNING;
    } else if (_logLevel == "notice") {
        m_logLevel = LL_NOTICE;
    } else if (_logLevel == "info") {
        m_logLevel = LL_INFO;
    } else if (_logLevel == "debug") {
        m_logLevel = LL_DEBUG;
    } else {
        std::cout << _logLevel << std::endl;
        throw std::runtime_error("wrong logging level");
    }
    
    if (_logTo == "syslog") {
        m_logDst = LD_SYSLOG;
        openlog(m_logPrefix.c_str(), 0, 0);
    } else if (_logTo == "console") {
        m_logDst = LD_CONSOLE;
    } else if (_logTo.length() > 0) {
        try {
            m_ofs.exceptions(std::ifstream::failbit | std::ifstream::badbit);
            m_ofs.open(_logTo, std::ios_base::out | std::ios_base::app);
            m_logDst = LD_FILE;
        } catch (...) {
            throw std::runtime_error("failed to open log file for writing");
        }
    } else {
        throw std::runtime_error("wrong logging destination");
    }
}

int logger_t::levelMapper(logLevel_t _logLevel) const {
    switch (_logLevel) {
        case LL_CRITICAL:
            return LOG_CRIT;
        case LL_ERROR:
            return LOG_ERR;
        case LL_WARNING:
            return LOG_WARNING;
        case LL_NOTICE:
            return LOG_NOTICE;
        case LL_INFO:
            return LOG_INFO;
        case LL_DEBUG:
            return LOG_DEBUG;
    }
    
    return LOG_DEBUG;
}


void logger_t::worker() {
    while (true) {
        logRecord_t logRecord;
        {
            std::unique_lock<std::mutex> lck(m_mtxLog);
            while (!m_checkQueueFlag) {
                m_cvLog.wait(lck);
            }

            if (!m_workFlag) {
                break;
            }

            if (!m_logQueue.empty()) {
                try {
                    logRecord = m_logQueue.front();
                    m_logQueue.pop();
                } catch (...) {
                    continue;
                }
            } else {
                m_checkQueueFlag = false;
                continue;
            }
            if (m_logQueue.empty()) {
                m_checkQueueFlag = false;
            }
        }
        try {
            auto recTime = std::chrono::system_clock::to_time_t(std::get<0>(logRecord));
            std::string recTimeStr = std::ctime(&recTime);
            if (recTimeStr[recTimeStr.length() - 1] == '\n') {
                recTimeStr.erase(recTimeStr.length() - 1);
            }
            if (m_logDst == LD_SYSLOG) {
                syslog(levelMapper(std::get<1>(logRecord)), "%s", std::get<2>(logRecord).c_str());
            } else if (m_logDst == LD_CONSOLE) {
                std::unique_lock<std::mutex> lck(m_mtxIO);
                std::cout << recTimeStr << ": " << std::get<2>(logRecord).c_str() << std::endl << std::flush;
            } else if (m_logDst == LD_FILE) {
                std::unique_lock<std::mutex> lck(m_mtxIO);
                m_ofs << recTimeStr << ": " << std::get<2>(logRecord).c_str() << std::endl << std::flush;
            }
        } catch(...) {}
    }
}
