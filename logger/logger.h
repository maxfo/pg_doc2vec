//
//  logger.h
//  kamgr
//
//  Created by Max Fomichev on 07/10/2016.
//  Copyright © 2016 Pieci Kvadrati. All rights reserved.
//

#ifndef logger_h
#define logger_h

#include <string>
#include <fstream>
#include <mutex>
#include <queue>
#include <tuple>
#include <chrono>
#include <thread>
#include <condition_variable>
#include <map>

#include "fmt/format.h"

class logger_t final {
public:
    enum logLevel_t {
        LL_CRITICAL,
        LL_ERROR,
        LL_WARNING,
        LL_NOTICE,
        LL_INFO,
        LL_DEBUG
    };
    
private:
    enum logDst_t {
        LD_SYSLOG,
        LD_CONSOLE,
        LD_FILE
    };
    const std::map<logLevel_t, std::string> m_logLevelStrs = {{LL_CRITICAL, "CRITICAL"},
                                                              {LL_ERROR, "ERROR"},
                                                              {LL_WARNING, "WARNING"},
                                                              {LL_NOTICE, "NOTICE"},
                                                              {LL_INFO, "INFO"},
                                                              {LL_DEBUG, "DEBUG"}};
//    uint16_t m_maxLogLineLength = std::numeric_limits<uint16_t>::max(); // Not less then m_logPrefix string length!
    std::string m_logPrefix;
    logDst_t m_logDst = LD_SYSLOG;
    logLevel_t m_logLevel = LL_ERROR;
    std::ofstream m_ofs;
    
    using logRecord_t = std::tuple<std::chrono::time_point<std::chrono::system_clock>, logLevel_t, std::string>;
    std::queue<logRecord_t> m_logQueue;
    std::mutex m_mtxLog;
    std::condition_variable m_cvLog;
    bool m_checkQueueFlag = false;

    std::thread m_workerThread;
    bool m_workFlag = true;
    std::mutex m_mtxIO;

    std::map<std::thread::id, uint64_t> m_dispatchThreads;

public:
    static logger_t &logger() {
        static logger_t logger;
        return logger;
    }
    
    logger_t(const logger_t &) = delete;
    void operator=(const logger_t &) = delete;
    logger_t(const logger_t &&) = delete;
    void operator=(const logger_t &&) = delete;
    ~logger_t();
    
    void init(const std::string &_logPrefix, const std::string &_logTo, const std::string &_logLevel);

    template <typename... args_t>
    void log(logLevel_t _logLevel, const std::string &_fmt, const args_t &..._args) {
        if (_logLevel > m_logLevel) {
            return;
        }

        try {
            std::unique_lock<std::mutex> lck(m_mtxLog);

            if (m_dispatchThreads.find(std::this_thread::get_id()) == m_dispatchThreads.end()) {
                m_dispatchThreads[std::this_thread::get_id()] = m_dispatchThreads.size();
            }

            std::string buffer = "[" + m_logPrefix + "] "
                                 + ((m_logLevel == LL_DEBUG)?
                                    "[THR#" + std::to_string(m_dispatchThreads[std::this_thread::get_id()]) + "] ":
                                    "")
                                 + "[" + m_logLevelStrs.at(_logLevel) + "] " + _fmt;

            fmt::MemoryWriter w;
            w.write(buffer, _args...);

            m_logQueue.push(logRecord_t(std::chrono::system_clock::now(), _logLevel, w.c_str()));
            m_checkQueueFlag = true;
            m_cvLog.notify_one();
        } catch(...) {}
    }

private:
    logger_t() {
        m_workerThread = std::thread(&logger_t::worker, this);
    }
    inline int levelMapper(logLevel_t) const;
    void worker();
};

#endif /* logger_h */
