#include <csignal>

#include <iostream>

#include "worker/listener.h"

void signalHandler() {
    sigset_t sigs;

    if ((sigemptyset(&sigs) != 0) ||
        (sigaddset(&sigs, SIGINT) != 0) || //exit
        (sigaddset(&sigs, SIGHUP) != 0) || //exit
        (sigaddset(&sigs, SIGQUIT) != 0) || //exit
        (sigaddset(&sigs, SIGTERM) != 0) || //exit
        (sigprocmask(SIG_BLOCK, &sigs, nullptr) != 0) ||
        (signal(SIGPIPE, SIG_IGN) == SIG_ERR)) {

        return;
    }

    while (true) {
        int sign = 0;
        if (sigwait(&sigs, &sign) != 0) {
            return;
        }

        switch (sign) {
            case SIGINT:
            case SIGHUP:
            case SIGQUIT:
            case SIGTERM:
                return;
            default:
                continue;
        }
    }
}

int main(int argc, char *argv[]) {
    if (argc != 5) {
        std::cerr << "Usage:" << std::endl
                  << "\t" << argv[0] << " [socket name] [reposytory path] [log file] [log level]" << std::endl;
        return EXIT_FAILURE;
    }

    auto &logger = logger_t::logger();
    logger.init("pg_doc2vec", argv[3], argv[4]);

    {
        try {
            std::unique_ptr<listener_t> listener(new listener_t(argv[1],
                                                                argv[2],
                                                                logger));
            signalHandler();
            return 0;
        } catch (const std::exception &_e) {
            std::cerr << _e.what() << std::endl;
        } catch (...) {
            std::cerr << "unknown error" << std::endl;
        }
    }

    return EXIT_FAILURE;
}
