#include <string>
#include <vector>
#include <iostream>

#include "client/d2v_client.h"

int main(int argc, char * const *argv) {

    std::unique_ptr<client_t> client;
    try {
        client.reset(new client_t());
        client->connect("/tmp/pg_doc2vec.socket");
    } catch (const std::exception &_e) {
        std::cerr << _e.what() << std::endl;
        return 1;
    } catch (...) {
        std::cerr << "unknown error on init" << std::endl;
        return 2;
    }

    if (argc == 3) {
        if (std::string(argv[1]) == "drop") {
            std::cout << client->drop(argv[2]) << std::endl;
            return 0;
        } else if (std::string(argv[1]) == "top_related") {
            std::vector<std::pair<std::size_t, std::size_t>> top;
            std::cout << client->topRelated(argv[2], 10, 0.728308, top) << std::endl;
            for (const auto &i:top) {
                std::cout << i.first << " " << i.second << std::endl;
            }
            return 0;
        }
    }
    if (argc == 4) {
        if (std::string(argv[1]) == "create") {
            std::cout << client->create(argv[2], std::stoi(argv[3])) << std::endl;
            return 0;
        } else if (std::string(argv[1]) == "erase") {
            std::cout << client->erase(argv[2], std::stoull(argv[3])) << std::endl;
            return 0;
        } else if (std::string(argv[1]) == "nearest_id") {
            std::vector<std::pair<std::size_t, float>> nearest;
            std::cout << client->nearest(argv[2], std::stoull(argv[3]), 3, nearest) << std::endl;
            for (const auto &i:nearest) {
                std::cout << i.first << " " << i.second << std::endl;
            }
            return 0;
        } else if (std::string(argv[1]) == "nearest_text") {
            std::vector<std::pair<std::size_t, float>> nearest;
            std::cout << client->nearest(argv[2], std::string(argv[3]), 3, nearest) << std::endl;
            for (const auto &i:nearest) {
                std::cout << i.first << " " << i.second << std::endl;
            }
            return 0;
        }
    }
    if (argc == 5) {
        if (std::string(argv[1]) == "set") {
            std::cout << client->set(argv[2], std::stoull(argv[3]), argv[4]) << std::endl;
            return 0;
        }
    }

    return 0;
}
