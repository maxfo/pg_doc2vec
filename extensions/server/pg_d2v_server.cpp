#include <d2v_server.h>

extern "C" {
#include <errno.h>

#include <postgres.h>
#include <fmgr.h>
#include <miscadmin.h>
#include <catalog/pg_type.h>
#include <postmaster/bgworker.h>
#include <storage/latch.h>
#include <storage/ipc.h>

PG_MODULE_MAGIC;

#ifndef SHARE_FOLDER
#error "SHARE_FOLDER must be defined"
#else
#define xstr(s) str(s)
#define str(s) #s
#define SHARE_FOLDER_STR xstr(SHARE_FOLDER)
#endif

static volatile sig_atomic_t doc2vecTerminated = false;

static void doc2vecSigterm(SIGNAL_ARGS) {
    int save_errno = errno;
    doc2vecTerminated = true;
    SetLatch(MyLatch);
    errno = save_errno;
}

void mainDoc2VecProc(Datum) {
    try {
        elog(LOG, "DOC2VEC: initializing logger");
        auto &logger = logger_t::logger();
        logger.init("pg_doc2vec", "/usr/local/var/log/pg_doc2vec.log", "debug");

        {
            elog(LOG, "DOC2VEC: initializing repository at %s", SHARE_FOLDER_STR);
            std::unique_ptr<listener_t> listener(new listener_t(SHARE_FOLDER_STR, logger));

            elog(LOG, "DOC2VEC: initialized");

            pqsignal(SIGTERM, doc2vecSigterm);
            BackgroundWorkerUnblockSignals();

            while (!doc2vecTerminated) {
                int rc = WaitLatch(MyLatch, WL_LATCH_SET | WL_TIMEOUT | WL_POSTMASTER_DEATH, 1000L);
                ResetLatch(MyLatch);
                if (rc & WL_POSTMASTER_DEATH) {
                    break;
                }
            }
        }
    } catch (const std::exception &_e) {
        elog(ERROR, "DOC2VEC: %s", _e.what());
    } catch (...) {
        elog(ERROR, "DOC2VEC: unknown error");
    }

    elog(LOG, "DOC2VEC: shutting down");
    proc_exit(0);
}

void _PG_init(void) {
    BackgroundWorker worker;

    sprintf(worker.bgw_library_name, "pg_d2v_server");
    sprintf(worker.bgw_function_name, "mainDoc2VecProc");
    snprintf(worker.bgw_name, BGW_MAXLEN, "doc2vec worker");
//    snprintf(worker.bgw_type, BGW_MAXLEN, "pg_d2v_server");

//    sprintf(worker.bgw_name, "doc2vec process");
//        worker.bgw_flags = BGWORKER_SHMEM_ACCESS;
    worker.bgw_start_time = BgWorkerStart_RecoveryFinished;
    worker.bgw_restart_time = BGW_NEVER_RESTART;
//    worker.bgw_main = mainDoc2VecProc;
    worker.bgw_notify_pid = 0;

    RegisterBackgroundWorker(&worker);
}
}
