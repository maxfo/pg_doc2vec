-- complain if script is sourced in psql, rather than via CREATE EXTENSION
\echo Use "CREATE EXTENSION pg_d2v_client" to load this file. \quit

CREATE FUNCTION d2v_create(text, int4) RETURNS boolean
AS '$libdir/pg_d2v_client'
LANGUAGE C IMMUTABLE STRICT;

CREATE FUNCTION d2v_drop(text) RETURNS boolean
AS '$libdir/pg_d2v_client'
LANGUAGE C IMMUTABLE STRICT;

CREATE FUNCTION d2v_set(text, int8, text) RETURNS boolean
AS '$libdir/pg_d2v_client'
LANGUAGE C IMMUTABLE STRICT;

CREATE FUNCTION d2v_erase(text, int8) RETURNS boolean
AS '$libdir/pg_d2v_client'
LANGUAGE C IMMUTABLE STRICT;

CREATE FUNCTION d2v_nearest_by_id(text, int8, int8, float4) RETURNS int8[]
AS '$libdir/pg_d2v_client'
LANGUAGE C IMMUTABLE STRICT;

CREATE FUNCTION d2v_nearest_by_text(text, text, int8, float4) RETURNS int8[]
AS '$libdir/pg_d2v_client'
LANGUAGE C IMMUTABLE STRICT;

CREATE FUNCTION d2v_top_related(text, int8, float4) RETURNS int8[]
AS '$libdir/pg_d2v_client'
LANGUAGE C IMMUTABLE STRICT;
