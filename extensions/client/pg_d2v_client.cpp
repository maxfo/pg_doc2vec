#include <vector>

#include <d2v_client.h>

extern "C" {
#include <errno.h>

#include <postgres.h>
#include <utils/array.h>
#include <catalog/pg_type.h>

PG_MODULE_MAGIC;

PG_FUNCTION_INFO_V1(d2v_create);
Datum d2v_create(PG_FUNCTION_ARGS) {
    if(PG_ARGISNULL(0) || PG_ARGISNULL(1)) {
        PG_RETURN_BOOL(false);
    }

    std::unique_ptr<client_t> client;
    try {
        // get ttl
        int32_t _ttl = PG_GETARG_INT32(1);

        // get repository name
        text *_name = PG_GETARG_TEXT_P(0);
        std::string name(VARDATA(_name), VARSIZE(_name) - VARHDRSZ);

        if (name.empty()) {
            PG_RETURN_BOOL(false);
        }

        client.reset(new client_t());
        client->connect("/tmp/pg_doc2vec.socket");
        PG_RETURN_BOOL(client->create(name, _ttl));
    } catch (const std::exception &_e) {
        elog(LOG, "DOC2VEC: d2v_create critical error: %s", _e.what());
    } catch (...) {
        elog(LOG, "DOC2VEC: d2v_create unknown critical error");
    }
    PG_RETURN_BOOL(false);
}

PG_FUNCTION_INFO_V1(d2v_drop);
Datum d2v_drop(PG_FUNCTION_ARGS) {
    if(PG_ARGISNULL(0)) {
        PG_RETURN_BOOL(false);
    }

    std::unique_ptr<client_t> client;
    try {
        // get repository name
        text *_name = PG_GETARG_TEXT_P(0);
        std::string name(VARDATA(_name), VARSIZE(_name) - VARHDRSZ);

        if (name.empty()) {
            PG_RETURN_BOOL(false);
        }

        client.reset(new client_t());
        client->connect("/tmp/pg_doc2vec.socket");
        PG_RETURN_BOOL(client->drop(name));
    } catch (const std::exception &_e) {
        elog(LOG, "DOC2VEC: d2v_drop critical error: %s", _e.what());
    } catch (...) {
        elog(LOG, "DOC2VEC: d2v_drop unknown critical error");
    }
    PG_RETURN_BOOL(false);
}

PG_FUNCTION_INFO_V1(d2v_set);
Datum d2v_set(PG_FUNCTION_ARGS) {
    if(PG_ARGISNULL(0) || PG_ARGISNULL(1) || PG_ARGISNULL(2)) {
        PG_RETURN_BOOL(false);
    }

    std::unique_ptr<client_t> client;
    try {
        // get repository name
        text *_name = PG_GETARG_TEXT_P(0);
        std::string name(VARDATA(_name), VARSIZE(_name) - VARHDRSZ);
        if (name.empty()) {
            PG_RETURN_BOOL(false);
        }
        // get id
        int64_t _id = PG_GETARG_INT64(1);
        if (_id == 0) {
            PG_RETURN_BOOL(false);
        }
        // get text
        text *_text = PG_GETARG_TEXT_P(2);
        std::string text(VARDATA(_text), VARSIZE(_text) - VARHDRSZ);
        if (text.empty()) {
            PG_RETURN_BOOL(false);
        }

        client.reset(new client_t());
        client->connect("/tmp/pg_doc2vec.socket");
        PG_RETURN_BOOL(client->set(name, _id, text));
    } catch (const std::exception &_e) {
        elog(LOG, "DOC2VEC: d2v_set critical error: %s", _e.what());
    } catch (...) {
        elog(LOG, "DOC2VEC: d2v_set unknown critical error");
    }
    PG_RETURN_BOOL(false);
}

PG_FUNCTION_INFO_V1(d2v_erase);
Datum d2v_erase(PG_FUNCTION_ARGS) {
    if(PG_ARGISNULL(0) || PG_ARGISNULL(1)) {
        PG_RETURN_BOOL(false);
    }

    std::unique_ptr<client_t> client;
    try {
        // get repository name
        text *_name = PG_GETARG_TEXT_P(0);
        std::string name(VARDATA(_name), VARSIZE(_name) - VARHDRSZ);
        if (name.empty()) {
            PG_RETURN_BOOL(false);
        }
        // get id
        int64_t _id = PG_GETARG_INT64(1);
        if (_id == 0) {
            PG_RETURN_BOOL(false);
        }

        client.reset(new client_t());
        client->connect("/tmp/pg_doc2vec.socket");
        PG_RETURN_BOOL(client->erase(name, _id));
    } catch (const std::exception &_e) {
        elog(LOG, "DOC2VEC: d2v_erase critical error: %s", _e.what());
    } catch (...) {
        elog(LOG, "DOC2VEC: d2v_erase unknown critical error");
    }
    PG_RETURN_BOOL(false);
}

PG_FUNCTION_INFO_V1(d2v_nearest_by_text);
Datum d2v_nearest_by_text(PG_FUNCTION_ARGS) {
    if (PG_ARGISNULL(0) || PG_ARGISNULL(1) || PG_ARGISNULL(2) || PG_ARGISNULL(3)) {
        PG_RETURN_NULL();
    }

    std::unique_ptr<client_t> client;
    try {
        // get repository name
        text *_name = PG_GETARG_TEXT_P(0);
        std::string name(VARDATA(_name), VARSIZE(_name) - VARHDRSZ);
        if (name.empty()) {
            PG_RETURN_NULL();
        }
        // get text
        text *_text = PG_GETARG_TEXT_P(1);
        std::string text(VARDATA(_text), VARSIZE(_text) - VARHDRSZ);
        if (text.empty()) {
            PG_RETURN_NULL();
        }
        // get limit
        int64_t _limit = PG_GETARG_INT64(2);
        if (_limit == 0) {
            PG_RETURN_NULL();
        }
        // get distance
        float _distance = PG_GETARG_FLOAT4(3);
        if (_distance <= 0.0f) {
            PG_RETURN_NULL();
        }

        client.reset(new client_t());
        client->connect("/tmp/pg_doc2vec.socket");
        std::vector<std::pair<std::size_t, float>> nearest;
        if (!client->nearest(name, text, _limit, nearest)) {
            PG_RETURN_NULL();
        }

        if (!nearest.empty()) {
            Datum ids[nearest.size()];
            int idx = 0;
            for (const auto &i:nearest) {
                if (i.second < _distance) {
                    break;
                }
                ids[idx] = Int64GetDatum(i.first);
                ++idx;
            }

            if (idx > 0) {
                ArrayType *array = construct_array(ids, idx, INT8OID, 8, true, 'd');
                PG_RETURN_ARRAYTYPE_P(array);
            }
        }
    } catch (const std::exception &_e) {
        elog(LOG, "DOC2VEC: d2v_nearest_by_text critical error: %s", _e.what());
    } catch (...) {
        elog(LOG, "DOC2VEC: d2v_nearest_by_text unknown critical error");
    }
    PG_RETURN_NULL();
}

PG_FUNCTION_INFO_V1(d2v_nearest_by_id);
Datum d2v_nearest_by_id(PG_FUNCTION_ARGS) {
    if (PG_ARGISNULL(0) || PG_ARGISNULL(1) || PG_ARGISNULL(2) || PG_ARGISNULL(3)) {
        PG_RETURN_NULL();
    }

    std::unique_ptr<client_t> client;
    try {
        // get repository name
        text *_name = PG_GETARG_TEXT_P(0);
        std::string name(VARDATA(_name), VARSIZE(_name) - VARHDRSZ);
        if (name.empty()) {
            PG_RETURN_NULL();
        }
        // get id
        int64_t _id = PG_GETARG_INT64(1);
        if (_id == 0) {
            PG_RETURN_NULL();
        }
        // get limit
        int64_t _limit = PG_GETARG_INT64(2);
        if (_limit == 0) {
            PG_RETURN_NULL();
        }
        // get distance
        float _distance = PG_GETARG_FLOAT4(3);
        if (_distance <= 0.0f) {
            PG_RETURN_NULL();
        }

        client.reset(new client_t());
        client->connect("/tmp/pg_doc2vec.socket");
        std::vector<std::pair<std::size_t, float>> nearest;
        if (!client->nearest(name, _id, _limit, nearest)) {
            PG_RETURN_NULL();
        }

        if (!nearest.empty()) {
            Datum ids[nearest.size()];
            int idx = 0;
            for (const auto &i:nearest) {
                if (i.second < _distance) {
                    break;
                }
                ids[idx] = Int64GetDatum(i.first);
                ++idx;
            }

            if (idx > 0) {
                ArrayType *array = construct_array(ids, idx, INT8OID, 8, true, 'd');
                PG_RETURN_ARRAYTYPE_P(array);
            }
        }
    } catch (const std::exception &_e) {
        elog(LOG, "DOC2VEC: d2v_nearest_by_text critical error: %s", _e.what());
    } catch (...) {
        elog(LOG, "DOC2VEC: d2v_nearest_by_text unknown critical error");
    }
    PG_RETURN_NULL();
}

PG_FUNCTION_INFO_V1(d2v_top_related);
Datum d2v_top_related(PG_FUNCTION_ARGS) {
    if (PG_ARGISNULL(0) || PG_ARGISNULL(1) || PG_ARGISNULL(2)) {
        PG_RETURN_NULL();
    }

    std::unique_ptr<client_t> client;
    try {
        // get repository name
        text *_name = PG_GETARG_TEXT_P(0);
        std::string name(VARDATA(_name), VARSIZE(_name) - VARHDRSZ);
        if (name.empty()) {
            PG_RETURN_NULL();
        }
        // get limit
        int64_t _limit = PG_GETARG_INT64(1);
        if (_limit == 0) {
            PG_RETURN_NULL();
        }
        // get distance
        float _distance = PG_GETARG_FLOAT4(2);
        if (_distance <= 0.0f) {
            PG_RETURN_NULL();
        }

        client.reset(new client_t());
        client->connect("/tmp/pg_doc2vec.socket");
        std::vector<std::pair<std::size_t, std::size_t>> top;
        if (!client->topRelated(name, _limit, _distance, top)) {
            PG_RETURN_NULL();
        }

        if (!top.empty()) {
            Datum ids[top.size()];
            int idx = 0;
            for (const auto &i:top) {
                ids[idx] = Int64GetDatum(i.first);
                ++idx;
            }

            if (idx > 0) {
                ArrayType *array = construct_array(ids, idx, INT8OID, 8, true, 'd');
                PG_RETURN_ARRAYTYPE_P(array);
            }
        }
    } catch (const std::exception &_e) {
        elog(LOG, "DOC2VEC: d2v_top_related critical error: %s", _e.what());
    } catch (...) {
        elog(LOG, "DOC2VEC: d2v_top_related unknown critical error");
    }
    PG_RETURN_NULL();
}
}
