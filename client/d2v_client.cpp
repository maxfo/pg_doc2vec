//
// Created by Max Fomichev on 25/07/2017.
//

#include <string.h>
#include <arpa/inet.h>
#include <sys/un.h>

#include <vector>
#include <cstring>
#include <chrono>

#include <event2/listener.h>
#include <event2/thread.h>
#include <event2/buffer.h>
#include <protobuf/client.h>

#include "d2v_client.h"

//#include <iostream>

client_t::client_t(): m_workerThread(), m_mtx(), m_cv(), m_mtxFailed() {
    evthread_use_pthreads();

    m_base = event_base_new();
    if (!m_base) {
        throw std::runtime_error("libevent init failed");
    }

    m_workerThread = std::thread(&client_t::worker, this);
}

client_t::~client_t() noexcept {
    event_base_loopexit(m_base, nullptr);
    m_workerThread.join();
    if (m_bev != nullptr) {
        bufferevent_free(m_bev);
    }
    event_base_free(m_base);
}

void client_t::connect(const std::string &_sockName)  {
    m_bev = bufferevent_socket_new(m_base, -1, BEV_OPT_CLOSE_ON_FREE | BEV_OPT_THREADSAFE);
    if (m_bev == nullptr) {
        throw std::runtime_error("proxy client: bufferevent_socket_new() failed");
    }

    struct sockaddr_un sun{};
    std::memset(&sun, 0, sizeof(sun));
    sun.sun_family = AF_LOCAL;
    std::memcpy(sun.sun_path, _sockName.data(), _sockName.length());

    if (bufferevent_socket_connect(m_bev, (struct sockaddr*) &sun, sizeof(sun)) < 0) {
        throw std::runtime_error("proxy client: bufferevent_socket_connect() failed");
    }

    bufferevent_setcb(m_bev,
                      [] (struct bufferevent *_bev, void *_ctx) {
//                          std::cout << "read event" << std::endl;
                          auto ec = static_cast<client_t *>(_ctx);

                          struct evbuffer *input = bufferevent_get_input(_bev);
                          auto ebLen = evbuffer_get_length(input);
                          std::vector<char> readBuf;
                          readBuf.resize(ebLen, 0);
                          evbuffer_remove(input, readBuf.data(), ebLen);
                          try {
                              if (ec->m_replyNearest) {
                                  ec->m_replyStatus = pb::client::reply_t(readBuf)(*ec->m_nearest);
                              } else if (ec->m_replyTop) {
                                      ec->m_replyStatus = pb::client::reply_t(readBuf)(*ec->m_top);
                              } else {
                                  ec->m_replyStatus = pb::client::reply_t(readBuf)();
                              }
                              std::unique_lock<std::mutex> lck(ec->m_mtx);
                              ec->m_checkFlag = true;
                              ec->m_cv.notify_one();
                          } catch (...) {
//                              std::cout << "pb failed" << std::endl;
                              // incomplete buffer? waiting for more data...
                          }
                      }, NULL,
                      [] (struct bufferevent *_bev, short _what, void *_ctx) {
                          auto ec = static_cast<client_t *>(_ctx);

                          if (_what & (BEV_EVENT_EOF | BEV_EVENT_ERROR | BEV_EVENT_TIMEOUT)) {
                              if (_what & BEV_EVENT_ERROR) {
                                  if (errno) {
//                                      std::cout << "io failed: " << errno << std::endl;
                                  } else {
//                                      std::cout << "io failed" << std::endl;
                                  }
                              } else if (_what & BEV_EVENT_EOF) {
//                                  std::cout << "connection closed" << std::endl;
                              } else {
//                                  std::cout << "io timeout" << std::endl;
                              }

                              bufferevent_free(_bev);
                              if (ec->m_bev == _bev) {
                                  ec->m_bev = nullptr;
                              }

                              {
                                  std::unique_lock<std::mutex> lck(ec->m_mtxFailed);
                                  ec->m_failed = true;
                              }
                              std::unique_lock<std::mutex> lck(ec->m_mtx);
                              ec->m_checkFlag = true;
                              ec->m_cv.notify_one();

                              return;
                          }

                          if (_what & (BEV_EVENT_CONNECTED)) {
                          }
                      },  this);

    struct timeval timeout{};
    timeout.tv_sec = m_nwTimeout;
    timeout.tv_usec = 0;
    bufferevent_set_timeouts(m_bev, &timeout, &timeout);
    bufferevent_enable(m_bev, EV_READ | EV_WRITE);
    {
        std::unique_lock<std::mutex> lck(m_mtxFailed);
        m_failed = false;
    }
}

void client_t::worker() noexcept {
//    std::cout << "worker started" << std::endl;
    auto ret = event_base_loop(m_base, EVLOOP_NO_EXIT_ON_EMPTY);
//    std::cout << "worker stopped" << std::endl;
    if (ret < 0) {
        return;
    }
}

bool client_t::create(const std::string &_name, uint32_t _ttl) {
    struct evbuffer *output = bufferevent_get_output(m_bev);
    pb::client::request_t request(_name);
    std::vector<char> data;
    request.create(_ttl, data);
    evbuffer_add(output, data.data(), data.size());

    // wait for server reply or error event
    std::unique_lock<std::mutex> lck(m_mtx);
    while (!m_checkFlag) {
        m_cv.wait(lck);
    }
    m_checkFlag = false;

    return m_replyStatus;
}

bool client_t::drop(const std::string &_name) {
    struct evbuffer *output = bufferevent_get_output(m_bev);
    pb::client::request_t request(_name);
    std::vector<char> data;
    request.drop(data);
    evbuffer_add(output, data.data(), data.size());

    // wait for server reply or error event
    std::unique_lock<std::mutex> lck(m_mtx);
    while (!m_checkFlag) {
        m_cv.wait(lck);
    }
    m_checkFlag = false;

    return m_replyStatus;
}

bool client_t::set(const std::string &_name, std::size_t _id, const std::string &_text) {
    struct evbuffer *output = bufferevent_get_output(m_bev);
    pb::client::request_t request(_name);
    std::vector<char> data;
    request.set(_id, _text, data);
    evbuffer_add(output, data.data(), data.size());

    // wait for server reply or error event
    std::unique_lock<std::mutex> lck(m_mtx);
    while (!m_checkFlag) {
        m_cv.wait(lck);
    }
    m_checkFlag = false;

    return m_replyStatus;
}

bool client_t::erase(const std::string &_name, std::size_t _id) {
    struct evbuffer *output = bufferevent_get_output(m_bev);
    pb::client::request_t request(_name);
    std::vector<char> data;
    request.erase(_id, data);
    evbuffer_add(output, data.data(), data.size());

    // wait for server reply or error event
    std::unique_lock<std::mutex> lck(m_mtx);
    while (!m_checkFlag) {
        m_cv.wait(lck);
    }
    m_checkFlag = false;

    return m_replyStatus;
}

bool client_t::nearest(const std::string &_name, std::size_t _id, std::size_t _limit,
             std::vector<std::pair<std::size_t, float>> &_nearest) {
    struct evbuffer *output = bufferevent_get_output(m_bev);
    pb::client::request_t request(_name);
    std::vector<char> data;
    request.nearest(_id, _limit, data);
    m_nearest = &_nearest;
    m_replyNearest = true;
    evbuffer_add(output, data.data(), data.size());

    // wait for server reply or error event
    std::unique_lock<std::mutex> lck(m_mtx);
    while (!m_checkFlag) {
        m_cv.wait(lck);
    }
    m_replyNearest = false;
    m_nearest = nullptr;
    m_checkFlag = false;

    return m_replyStatus;
}

bool client_t::nearest(const std::string &_name, const std::string &_text, std::size_t _limit,
             std::vector<std::pair<std::size_t, float>> &_nearest) {
    struct evbuffer *output = bufferevent_get_output(m_bev);
    pb::client::request_t request(_name);
    std::vector<char> data;
    request.nearest(_text, _limit, data);
    m_nearest = &_nearest;
    m_replyNearest = true;
    evbuffer_add(output, data.data(), data.size());

    // wait for server reply or error event
    std::unique_lock<std::mutex> lck(m_mtx);
    while (!m_checkFlag) {
        m_cv.wait(lck);
    }
    m_replyNearest = false;
    m_nearest = nullptr;
    m_checkFlag = false;

    return m_replyStatus;
}

bool client_t::topRelated(const std::string &_name, std::size_t _limit, float _minDistance,
                std::vector<std::pair<std::size_t, std::size_t>> &_top) {
    struct evbuffer *output = bufferevent_get_output(m_bev);
    pb::client::request_t request(_name);
    std::vector<char> data;
    request.topRelated(_limit, _minDistance, data);
    m_top = &_top;
    m_replyTop = true;
    evbuffer_add(output, data.data(), data.size());

    // wait for server reply or error event
    std::unique_lock<std::mutex> lck(m_mtx);
    while (!m_checkFlag) {
        m_cv.wait(lck);
    }
    m_replyTop = false;
    m_top = nullptr;
    m_checkFlag = false;

    return m_replyStatus;
}
