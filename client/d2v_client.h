//
// Created by Max Fomichev on 25/07/2017.
//

#ifndef PG_DOC2VEC_CLN_CLIENT_H
#define PG_DOC2VEC_CLN_CLIENT_H

#include <string>
#include <thread>
#include <mutex>
#include <condition_variable>

#include <event2/bufferevent.h>

class client_t final {
private:
    struct event_base *m_base = nullptr;
    struct bufferevent *m_bev = nullptr;
    std::thread m_workerThread;
    std::mutex m_mtx;
    std::condition_variable m_cv;
    bool m_checkFlag = false;

    std::mutex m_mtxFailed;
    bool m_failed = true;
    const uint8_t m_nwTimeout = 30;
    bool m_replyStatus = false;
    bool m_replyNearest = false;
    bool m_replyTop = false;
    std::vector<std::pair<std::size_t, float>> *m_nearest = nullptr;
    std::vector<std::pair<std::size_t, std::size_t>> *m_top = nullptr;

public:
    client_t();
    client_t(const client_t &) = delete;
    void operator=(const client_t &) = delete;
    ~client_t() noexcept;

    void connect(const std::string &_sockName);
    bool connected() const noexcept {return !m_failed;}

    bool create(const std::string &_name, uint32_t _ttl);
    bool drop(const std::string &_name);
    bool set(const std::string &_name, std::size_t _id, const std::string &_text);
    bool erase(const std::string &_name, std::size_t _id);
    bool nearest(const std::string &_name, std::size_t _id, std::size_t _limit,
                 std::vector<std::pair<std::size_t, float>> &_nearest);
    bool nearest(const std::string &_name, const std::string &_text, std::size_t _limit,
                 std::vector<std::pair<std::size_t, float>> &_nearest);
    bool topRelated(const std::string &_name, std::size_t _limit, float _minDistance,
                    std::vector<std::pair<std::size_t, std::size_t>> &_top);

private:
    void worker() noexcept;
};

#endif //PG_DOC2VEC_CLN_CLIENT_H
