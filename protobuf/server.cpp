//
// Created by Max Fomichev on 10/05/2017.
//

#include "d2v.pb.h"
#include "server.h"

namespace pb {
    namespace server {
        void request_t::operator()(const std::vector<char> &_requestMsg, std::vector<char> &_replyMsg, void *_ctx) {
            d2v_proto::request request;
            request.ParseFromArray(_requestMsg.data(), static_cast<int>(_requestMsg.size()));

            bool ret = false;
            d2v_proto::reply reply;
            std::vector<std::pair<std::size_t, float>> nearest;
            std::vector<std::pair<std::size_t, std::size_t>> top;

            switch (request.cmd()) {
                case d2v_proto::CREATE: {
                    auto &data = request.createdata();
                    ret = m_serverCallbacks.scbCreate(request.name(), data.ttl(), _ctx);
                    break;
                }
                case d2v_proto::DROP: {
                    ret = m_serverCallbacks.scbDrop(request.name(), _ctx);
                    break;
                }
                case d2v_proto::SET: {
                    auto &data = request.setdata();
                    ret = m_serverCallbacks.scbSet(request.name(), data.id(), data.text(), _ctx);
                    break;
                }
                case d2v_proto::ERASE: {
                    auto &data = request.erasedata();
                    ret = m_serverCallbacks.scbErase(request.name(), data.id(), _ctx);
                    break;
                }
                case d2v_proto::NEAREST_ID: {
                    auto &data = request.nearestiddata();
                    ret = m_serverCallbacks.scbNearestID(request.name(), data.id(), data.limit(), nearest, _ctx);
                    break;
                }
                case d2v_proto::NEAREST_TEXT: {
                    auto &data = request.nearesttextdata();
                    ret = m_serverCallbacks.scbNearestText(request.name(), data.text(), data.limit(), nearest, _ctx);
                    break;
                }
                case d2v_proto::TOP_RELATED: {
                    auto &data = request.toprelateddata();
                    ret = m_serverCallbacks.scbTopRelated(request.name(), data.limit(), data.distance(), top, _ctx);
                    break;
                }
                case d2v_proto::UNKNOWN:
                default: {
                }
            }

            reply.set_cmd(request.cmd());
            reply.set_status(ret);
            for (const auto &i:nearest) {
                auto n = reply.add_nearest();
                n->set_id(i.first);
                n->set_distance(i.second);
            }
            for (const auto &i:top) {
                auto n = reply.add_toprelated();
                n->set_id(i.first);
                n->set_amount(i.second);
            }
            auto size = reply.ByteSizeLong();
            _replyMsg.resize(size);
            reply.SerializeToArray(_replyMsg.data(), static_cast<int>(size));
        }
    }
}
