//
// Created by Max Fomichev on 10/05/2017.
//

#ifndef PG_DOC2VEC_PB_CLIENT_H
#define PG_DOC2VEC_PB_CLIENT_H

#include <string>
#include <vector>
#include <functional>

namespace pb {
    namespace client {
        class reply_t {
        private:
            const std::vector<char> &m_msg;

        public:
            explicit reply_t(const std::vector<char> &_msg): m_msg(_msg) {}
            ~reply_t() = default;

            bool operator()() const;
            bool operator()(std::vector<std::pair<std::size_t, float>>  &_nearest) const;
            bool operator()(std::vector<std::pair<std::size_t, std::size_t>> &_top) const;
        };

        class request_t {
        private:
            std::string m_name;

        public:
            explicit request_t(std::string _name) : m_name(std::move(_name)) {}
            ~request_t() = default;

            void create(uint32_t _ttl, std::vector<char> &_rawData);
            void drop(std::vector<char> &_rawData);
            void set(std::size_t _id, const std::string &_text, std::vector<char> &_rawData);
            void erase(std::size_t _id, std::vector<char> &_rawData);
            void nearest(std::size_t _id, std::size_t _limit, std::vector<char> &_rawData);
            void nearest(const std::string &_text, std::size_t _limit, std::vector<char> &_rawData);
            void topRelated(std::size_t _limit, float _distance, std::vector<char> &_rawData);
        };
    }
}

#endif //PG_DOC2VEC_CLIENT_H
