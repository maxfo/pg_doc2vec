//
// Created by Max Fomichev on 10/05/2017.
//

#include "d2v.pb.h"
#include "client.h"

namespace pb {
    namespace client {
        void request_t::create(uint32_t _ttl, std::vector<char> &_rawData) {
            d2v_proto::request msg;
            msg.set_cmd(d2v_proto::CREATE);
            msg.set_name(m_name);

            msg.mutable_createdata()->set_ttl(_ttl);

            auto size = msg.ByteSize();
            _rawData.resize(static_cast<std::size_t>(size));
            msg.SerializeToArray(_rawData.data(), size);
        }

        void request_t::drop(std::vector<char> &_rawData) {
            d2v_proto::request msg;
            msg.set_cmd(d2v_proto::DROP);
            msg.set_name(m_name);

            auto size = msg.ByteSize();
            _rawData.resize(static_cast<std::size_t>(size));
            msg.SerializeToArray(_rawData.data(), size);
        }

        void request_t::set(std::size_t _id, const std::string &_text, std::vector<char> &_rawData) {
            d2v_proto::request msg;
            msg.set_cmd(d2v_proto::SET);
            msg.set_name(m_name);

            msg.mutable_setdata()->set_id(_id);
            msg.mutable_setdata()->set_text(_text);

            auto size = msg.ByteSize();
            _rawData.resize(static_cast<std::size_t>(size));
            msg.SerializeToArray(_rawData.data(), size);
        }

        void request_t::erase(std::size_t _id, std::vector<char> &_rawData) {
            d2v_proto::request msg;
            msg.set_cmd(d2v_proto::ERASE);
            msg.set_name(m_name);

            msg.mutable_erasedata()->set_id(_id);

            auto size = msg.ByteSize();
            _rawData.resize(static_cast<std::size_t>(size));
            msg.SerializeToArray(_rawData.data(), size);
        }

        void request_t::nearest(std::size_t _id, std::size_t _limit, std::vector<char> &_rawData) {
            d2v_proto::request msg;
            msg.set_cmd(d2v_proto::NEAREST_ID);
            msg.set_name(m_name);

            msg.mutable_nearestiddata()->set_id(_id);
            msg.mutable_nearestiddata()->set_limit(_limit);

            auto size = msg.ByteSize();
            _rawData.resize(static_cast<std::size_t>(size));
            msg.SerializeToArray(_rawData.data(), size);
        }

        void request_t::nearest(const std::string &_text, std::size_t _limit, std::vector<char> &_rawData) {
            d2v_proto::request msg;
            msg.set_cmd(d2v_proto::NEAREST_TEXT);
            msg.set_name(m_name);

            msg.mutable_nearesttextdata()->set_text(_text);
            msg.mutable_nearesttextdata()->set_limit(_limit);

            auto size = msg.ByteSize();
            _rawData.resize(static_cast<std::size_t>(size));
            msg.SerializeToArray(_rawData.data(), size);
        }

        void request_t::topRelated(std::size_t _limit, float _distance, std::vector<char> &_rawData) {
            d2v_proto::request msg;
            msg.set_cmd(d2v_proto::TOP_RELATED);
            msg.set_name(m_name);

            msg.mutable_toprelateddata()->set_limit(_limit);
            msg.mutable_toprelateddata()->set_distance(_distance);

            auto size = msg.ByteSize();
            _rawData.resize(static_cast<std::size_t>(size));
            msg.SerializeToArray(_rawData.data(), size);
        }

        bool reply_t::operator()() const {
            d2v_proto::reply reply;
            reply.ParseFromArray(m_msg.data(), static_cast<int>(m_msg.size()));

            return reply.status();
        }

        bool reply_t::operator()(std::vector<std::pair<std::size_t, float>>  &_nearest) const {
            d2v_proto::reply reply;
            reply.ParseFromArray(m_msg.data(), m_msg.size());

            for (int i = 0; i < reply.nearest_size(); ++i) {
                _nearest.emplace_back(std::pair<std::size_t, float>(reply.nearest(i).id(),
                                                                    reply.nearest(i).distance()));
            }

            return reply.status();
        }

        bool reply_t::operator()(std::vector<std::pair<std::size_t, std::size_t>> &_top) const {
            d2v_proto::reply reply;
            reply.ParseFromArray(m_msg.data(), m_msg.size());

            for (int i = 0; i < reply.toprelated_size(); ++i) {
                _top.emplace_back(std::pair<std::size_t, std::size_t>(reply.toprelated(i).id(),
                                                                    reply.toprelated(i).amount()));
            }

            return reply.status();
        }
    }
}
