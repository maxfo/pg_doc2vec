//
// Created by Max Fomichev on 10/05/2017.
//

#ifndef PG_DOC2VEC_PB_SERVER_H
#define PG_DOC2VEC_PB_SERVER_H

#include <string>
#include <vector>
#include <functional>

namespace pb {
    namespace server {
        class request_t final {
        public:
            using scbCreate_t = std::function<bool(const std::string &_name,
                                                   uint32_t _ttl,
                                                   void *_ctx)>;

            using scbDrop_t = std::function<bool(const std::string &_name,
                                                 void *_ctx)>;

            using scbSet_t = std::function<bool(const std::string &_name,
                                                std::size_t _id,
                                                const std::string &_text,
                                                void *_ctx)>;

            using scbErase_t = std::function<bool(const std::string &_name,
                                                  std::size_t _id,
                                                  void *_ctx)>;

            using scbNearestID_t = std::function<bool(const std::string &_name,
                                                      std::size_t _id,
                                                      std::size_t _limit,
                                                      std::vector<std::pair<std::size_t, float>> &_nearest,
                                                      void *_ctx)>;

            using scbNearestText_t = std::function<bool(const std::string &_name,
                                                        const std::string &_text,
                                                        std::size_t _limit,
                                                        std::vector<std::pair<std::size_t, float>> &_nearest,
                                                        void *_ctx)>;

            using scbTopRelated_t = std::function<bool(const std::string &_name,
                                                       std::size_t _limit,
                                                       float _minDistance,
                                                       std::vector<std::pair<std::size_t, std::size_t>> &_top,
                                                       void *_ctx)>;

            struct serverCallbacks_t {
                scbCreate_t scbCreate;
                scbDrop_t scbDrop;
                scbSet_t scbSet;
                scbErase_t scbErase;
                scbNearestID_t scbNearestID;
                scbNearestText_t scbNearestText;
                scbTopRelated_t scbTopRelated;
            };

        private:
            serverCallbacks_t m_serverCallbacks;

        public:
            explicit request_t(serverCallbacks_t _serverCallbacks):
                    m_serverCallbacks(std::move(_serverCallbacks)) {}

            ~request_t() = default;

            void operator()(const std::vector<char> &_requestMsg, std::vector<char> &_replyMsg, void *_ctx);
        };
    }
}

#endif //PG_DOC2VEC_PB_SERVER_H
